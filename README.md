# Rosetta POS Framework

The code doing the heavy lifting of building HTML Pages, JavaScript and CSS files. Language versioning and security is also handled here.


## Change

After making changes don't forget to run `node deployer.js` which will update `dist.js` file.


## Use as a Node Module

To add framework dependency to your application include following line to dependencies in `package.json`:
```
   ...
   "unique-framework": "git@bitbucket.org:uniquedigital/rosetta-pos-framework.git#TAG"
   ...
```
Substitute TAG with some existing tag name, i.e. `1.0.0-docker-bower-npm-poc`. Tag defines a framework version you want to
use in your application. During `npm install` package manager will download the unique-framework to `node_modules` directory.

To make that work you must use Bitbucket's RSA key authentication mechanism instead of username/password.

If specified without a tag (#TAG), the latest, most probably, unstable version from master will be downloaded.
In this case don't forget to `npm update` in order to get latest commits.


## Release

When you're done with changes in framework, when it's tested and stable, it's time to make a release. Currently, this means
tagging some commit in Git.

```
git tag -a 1.0.4 -m "Release 1.0.4"
git push --tags
```

The command above will create a tag and push it to Bitbucket. After that, version can be updated to 1.0.4 in dependent projects,
and `npm update` executed to download the update.


### Who do I talk to?

* Greg Bridle
* greg@mailudc.com
* Skype: greg.bridle
* Phone: +36 30 300 5949

* Dmitrij Matulevič
* dmitrij@mailudc.com
