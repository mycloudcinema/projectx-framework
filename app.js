"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.validateLanguage = void 0;
var VERSION_NUMBER = '0.0.18';
var express = require("express");
var Utils = require("./factory/Utils");
var Request = require("./factory/Request");
var compress = require("compression");
var favicon = require("serve-favicon");
var SESSION = require("express-session");
var PageManager = Request.PageManager;
var fs = require("fs-extra");
var bodyParser = require("body-parser");
var fwSession = require("./factory/Session");
var sessionStore = require("express-mysql-session");
var WebservicesManager = Request.WebservicesManager;
var Settings = require("./external/Settings");
var cors = require("cors");
var multer = require("multer");
var uuid = require("node-uuid");
var path = require("path");
var URL = require("url");
var Logger_1 = require("./external/Logger");
var cookieParser = require("cookie-parser");
var settings = Settings;
var log = new Utils.Logger("app", 0).log;
var logger = Logger_1.getLogger("Application index", settings);
try {
    require('pmx').init({
        http: true,
        ignore_routes: [],
        errors: true,
        custom_probes: true,
        network: true,
        ports: false
    });
}
catch (error) {
    logger.error('Failed to load module pmx', error);
}
var App;
(function (App) {
    var port = settings.app.port_number;
    logger.info("Starting Project X Framework Version " + VERSION_NUMBER);
    init();
    var app = express();
    var pageManager = new PageManager(__dirname);
    var webserviceManager = new WebservicesManager(__dirname);
    var sessionManager = new fwSession.SessionManager();
    app.use(SESSION({
        name: "NSID",
        secret: "C'mon! This CAN'T be the self-destruct button. If it was, they wouldn' leave it lying around like this where anyone could push it! ",
        resave: true,
        rolling: true,
        saveUninitialized: true,
        cookie: { secure: false, maxAge: false },
        expires: "20m",
        store: new sessionStore(settings.session.sessionStoreConnection)
    }));
    app.use(favicon(process.cwd() + "/../public/images/favicon.ico"));
    app.use(compress());
    app.use(cookieParser());
    function elapedTime(start) {
        var elapsed = process.hrtime(start);
        return 1000 * elapsed[0] + elapsed[1] / 1000000;
    }
    app.use(function (req, res, next) {
        var start = process.hrtime();
        res.on('finish', function () {
            logger.debug("Response generated in " + elapedTime(start) + "ms for " + req.url, '');
        });
        next();
    });
    app.enable('trust proxy');
    app.use(bodyParser.json({ limit: '128mb' }));
    app.use(bodyParser.urlencoded({ extended: true, limit: '128mb' }));
    app.use("/public", express.static(process.cwd() + "/../public", { maxAge: 0 }));
    logger.info("Serving static resources at: /public");
    app.use("/media", express.static(settings.resource.mediaFolderPath));
    logger.info("Serving media at: " + settings.resource.mediaFolderPath);
    var upload = multer({
        storage: multer.memoryStorage()
    });
    app.use(cors());
    app.post('/upload', upload.array('files'), uploadRoute);
    function uploadRoute(req, res) {
        res.json({
            files: req.files.map(function (file) {
                console.log('Processing file', file);
                var uploadPath = settings.resource.mediaFolderPath + "/videos/";
                var fileName = uuid.v4() + path.extname(file.originalname);
                console.log('Saving to', uploadPath + fileName);
                fs.mkdirs(uploadPath, function (err) {
                    if (err) {
                        console.error(err);
                    }
                    else {
                        fs.writeFile(uploadPath + fileName, file.buffer, function (err) {
                            if (err) {
                                console.error(err);
                            }
                            else {
                                console.log('Upload', file);
                                delete file.buffer;
                            }
                        });
                    }
                });
                return {
                    uploadPath: uploadPath,
                    fileName: fileName,
                    file: file
                };
            })
        });
    }
    app.get("/webservices/*", webserviceManager.doGet);
    app.post("/webservices/*", webserviceManager.doPost);
    app.post("/elevate/webservices/*", function (request, response) {
        var new_url = request.url.replace("/elevate", "");
        var query = URL.parse(new_url);
        console.log(query);
        sessionManager.grantElevatedRight(request, query.pathname.replace("/webservices/", ""), function (a) {
            console.log(a);
            response.redirect(307, request.url.replace("/elevate", ""));
        }, function () {
        });
    });
    app.get("/session/get", function (request, response) {
        if (sessionManager.isLoggedIn(request)) {
            response.json({ data: sessionManager.getUserSessionData(request), resultCode: 0 });
        }
    });
    app.post("/session/set", function (request, response) {
        if (sessionManager.isLoggedIn(request)) {
            sessionManager.setUserSessionData(request, request.body);
            response.json({ data: sessionManager.getUserSessionData(request), resultCode: 0 });
        }
        else {
            response.json({ data: "Are you logged in?", resultCode: 1 });
        }
    });
    app.get("/logout", function (request, response) {
        request.session.destroy(function (error) {
            logger.info("Session destroyed");
            response.redirect(settings.template.loginTemplateName || "/base/login");
        });
    });
    var LANGUAGE_CHECK_REGEXP = /^\/(\w{2})($|\/)/;
    app.get('/*', function (request, response) {
        var parsedUrl = URL.parse(request.url);
        var match = parsedUrl.pathname.match(LANGUAGE_CHECK_REGEXP);
        if (match !== null) {
            request.url = request.url.replace(LANGUAGE_CHECK_REGEXP, '/');
            request.session.language = validateLanguage(match[1].toUpperCase());
        }
        else {
            if (typeof request.session.language === 'undefined') {
                request.session.language = settings.template.defaultLanguage;
            }
            else {
                request.session.language = validateLanguage(request.session.language);
            }
        }
        logger.info("Language set to " + request.session.language, request.session.user);
        pageManager.doGet(request, response);
    });
    app.post("/login", function (request, response) {
        sessionManager.Login(request, response, function (user) {
            var tmp;
            if (request.session.redirect_to) {
                tmp = request.session.redirect_to;
                delete request.session.redirect_to;
            }
            if (tmp === '/') {
                tmp = user.default_page;
            }
            logger.info("User is now logged in", request.session.user);
            response.json({ redirect: tmp ? tmp : user.default_page, resultCode: 0 });
        }, function () {
            response.statusCode = 401;
            response.json({
                data: "Login failed",
                resultCode: 1
            });
        });
    });
    var server = app.listen(port, function () {
        logger.info("Application running at port: " + port);
    });
})(App || (App = {}));
function init() {
    if (!fs.existsSync(process.cwd() + "/../public")) {
        log("[app.js] Could not locate public folder in application root.", Utils.Severity.medium);
        fs.mkdir(process.cwd() + "/../public");
        log("/public created");
        fs.mkdir(process.cwd() + "/../public/generated");
        log("/public/generated created");
    }
    else if (!fs.existsSync(process.cwd() + "/../public/generated")) {
        log("Could not locate folder for generated resources in public.", Utils.Severity.medium);
        fs.mkdir(process.cwd() + "/../public/generated");
        log("/public/generated created");
    }
}
function validateLanguage(language) {
    logger.debug("Validating language " + language + " against supported languages [" + settings.template.supportedLanguages + "]");
    if (settings.template.supportedLanguages.indexOf(language) === -1) {
        logger.warn("The selected language (" + language + ") is invalid or not supported. Switching to the default (" + settings.template.defaultLanguage + ")");
        return settings.template.defaultLanguage;
    }
    return language;
}
exports.validateLanguage = validateLanguage;
