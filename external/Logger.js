"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ts_log_debug_1 = require("ts-log-debug");
var NO_USER_TAG = "-";
function getLogger(environment, settings) {
    return new Log(environment, settings);
}
exports.getLogger = getLogger;
var Log = (function () {
    function Log(environment, settings) {
        this.prefix = "[" + environment + "]\t\t\t";
        if (typeof settings === "undefined") {
            this.settings = {
                app: {
                    log_level: 3
                }
            };
        }
        else {
            this.settings = settings;
        }
    }
    Log.prototype.trace = function (message, user) {
        if (this.settings.app.log_level < 4)
            return;
        ts_log_debug_1.$log.trace(this.prefix, message, "[" + (user ? user.login + ' ' + user.user_id : NO_USER_TAG) + "]");
    };
    Log.prototype.debug = function (message, user) {
        if (this.settings.app.log_level < 3)
            return;
        ts_log_debug_1.$log.debug(this.prefix, message, "[" + (user ? user.login + ' ' + user.user_id : NO_USER_TAG) + "]");
    };
    Log.prototype.info = function (message, user) {
        if (this.settings.app.log_level < 2)
            return;
        ts_log_debug_1.$log.info(this.prefix, message, "[" + (user ? user.login + ' ' + user.user_id : NO_USER_TAG) + "]");
    };
    Log.prototype.warn = function (message, user) {
        if (this.settings.app.log_level < 1)
            return;
        ts_log_debug_1.$log.warn(this.prefix, message, "[" + (user ? user.login + ' ' + user.user_id : NO_USER_TAG) + "]");
    };
    Log.prototype.error = function (message, user) {
        ts_log_debug_1.$log.error(this.prefix, message, "[" + (user ? user.login + ' ' + user.user_id : NO_USER_TAG) + "]");
    };
    return Log;
}());
exports.Log = Log;
