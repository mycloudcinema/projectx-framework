/*
  ____                     _
 |  _ \ ___ _ __ ___   ___| | __
 | |_) / _ \ '_ ` _ \ / _ \ |/ /
 |  _ <  __/ | | | | |  __/   <
 |_| \_\___|_| |_| |_|\___|_|\_\

   ____                      _ _   _
  / ___|___  _ __  ___ _   _| | |_(_)_ __   __ _
 | |   / _ \| '_ \/ __| | | | | __| | '_ \ / _` |
 | |__| (_) | | | \__ \ |_| | | |_| | | | | (_| |
  \____\___/|_| |_|___/\__,_|_|\__|_|_| |_|\__, |
                                            |___/

*/
import { $log } from "ts-log-debug";

const NO_USER_TAG = "-"; // The tag to be used for user login replacement for logging without user sessions.

/**
 * Logger factory function. Returns a Logger instance for the given environment.
 * @param environment
 * @returns {Log}
 */
export function getLogger(environment:any, settings:any):Log {
	return new Log(environment, settings);
}

export class Log {

	private readonly prefix: string;
	private settings: any;

	constructor(environment:any, settings:any) {
		this.prefix = `[${environment}]			`;
		if (typeof settings === "undefined") {
			// Default log settings.
			this.settings = {
				app: {
					log_level: 3
				}
			};
		}
		else {
			this.settings = settings;
		}
	}

	public trace(message:string, user?:any) {
		if (this.settings.app.log_level < 4) return;
		$log.trace(this.prefix, message, "[" + (user ? user.login + ' ' + user.user_id : NO_USER_TAG) + "]");
	}
	public debug(message:string, user?:any) {
		if (this.settings.app.log_level < 3) return;
		$log.debug(this.prefix, message, "[" + (user ? user.login + ' ' + user.user_id : NO_USER_TAG) + "]");
	}
	public info(message:string, user?:any) {
		if (this.settings.app.log_level < 2) return;
		$log.info(this.prefix, message, "[" + (user ? user.login + ' ' + user.user_id : NO_USER_TAG) + "]");
	}
	public warn(message:string, user?:any) {
		if (this.settings.app.log_level < 1) return;
		$log.warn(this.prefix, message, "[" + (user ? user.login + ' ' + user.user_id : NO_USER_TAG) + "]");
	}
	public error(message:string, user?:any) {
		$log.error(this.prefix, message, "[" + (user ? user.login + ' ' + user.user_id : NO_USER_TAG) + "]");
	}
}
