"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var fs = require("fs");
var path = require("path");
var Logger_1 = require("../external/Logger");
var logger = Logger_1.getLogger("Settings Manager", self);
var self = this;
var SETTINGS_DIRECTORY_PATH = "../settings";
var HOSTNAME = require('os').hostname();
global.template = {
    "defaultBaseTemplateName": null,
    "assetPath": null,
    "defaultLanguage": null,
    "supportedLanguages": null
};
var watchingDefaultSettingsFile = false;
var watchingLocalSettingsFile = false;
function loadSettingsFiles() {
    var defaultSettings;
    var localSettings;
    var defaultSettingsFile = path.join(process.cwd(), SETTINGS_DIRECTORY_PATH, '/default.json');
    var localSettingsFile = locateCustomSettingsFile();
    try {
        defaultSettings = JSON.parse(fs.readFileSync(defaultSettingsFile, 'utf8'));
    }
    catch (error) {
        logger.error("Could not locate default settings file \"" + defaultSettingsFile + "\" or the file is not valid JSON", error);
    }
    try {
        localSettings = JSON.parse(fs.readFileSync(localSettingsFile, 'utf8'));
    }
    catch (error) {
    }
    if (typeof defaultSettings !== 'undefined') {
        if (!watchingDefaultSettingsFile) {
            watchingDefaultSettingsFile = true;
            fs.watchFile(defaultSettingsFile, function (current, previous) {
                logger.info("Detected a change in settings.json. Reloading settings...");
                loadSettingsFiles();
            });
        }
        if (typeof localSettings !== 'undefined') {
            defaultSettings = deepExtend(defaultSettings, localSettings);
            logger.info("Extending default settings with settings for " + HOSTNAME);
            if (!watchingLocalSettingsFile) {
                watchingLocalSettingsFile = true;
                fs.watchFile(localSettingsFile, function (current, previous) {
                    logger.info("Detected a change in settings.json. Reloading settings...");
                    loadSettingsFiles();
                });
            }
        }
        for (var key in defaultSettings) {
            self[key] = defaultSettings[key];
        }
        logger.info('Settings module loaded');
    }
}
function locateCustomSettingsFile() {
    var customSettingsFile = process.argv[2];
    var localSettingsFile = path.join(process.cwd(), SETTINGS_DIRECTORY_PATH, "/" + HOSTNAME + ".json");
    if (customSettingsFile && fs.existsSync(customSettingsFile)) {
        logger.info("Using custom settings file: " + customSettingsFile + ".");
        return customSettingsFile;
    }
    else if (fs.existsSync(localSettingsFile)) {
        logger.info("Using local settings file: " + localSettingsFile + ".");
        return localSettingsFile;
    }
    else {
        if (customSettingsFile) {
            logger.warn("Neither local settings file '" + localSettingsFile + "' nor custom settings file '" + customSettingsFile + "' were found.");
        }
        else {
            logger.warn("Local settings file '" + localSettingsFile + "' was not found.");
        }
        return undefined;
    }
}
{
    loadSettingsFiles();
}
function deepExtend() {
    var args = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        args[_i] = arguments[_i];
    }
    var target = args[0];
    var val, src, clone;
    args.forEach(function (obj) {
        if (typeof obj !== 'object' || Array.isArray(obj)) {
            return;
        }
        Object.keys(obj).forEach(function (key) {
            src = target[key];
            val = obj[key];
            if (val === target) {
                return;
            }
            else if (typeof val !== 'object' || val === null) {
                target[key] = val;
                return;
            }
            else if (Array.isArray(val)) {
                target[key] = deepCloneArray(val);
                return;
            }
            else if (isSpecificValue(val)) {
                target[key] = cloneSpecificValue(val);
                return;
            }
            else if (typeof src !== 'object' || src === null || Array.isArray(src)) {
                target[key] = deepExtend({}, val);
                return;
            }
            else {
                target[key] = deepExtend(src, val);
                return;
            }
        });
    });
    return target;
}
function deepCloneArray(arr) {
    var clone = [];
    arr.forEach(function (item, index) {
        if (typeof item === 'object' && item !== null) {
            if (Array.isArray(item)) {
                clone[index] = deepCloneArray(item);
            }
            else if (isSpecificValue(item)) {
                clone[index] = cloneSpecificValue(item);
            }
            else {
                clone[index] = deepExtend({}, item);
            }
        }
        else {
            clone[index] = item;
        }
    });
    return clone;
}
function isSpecificValue(val) {
    return !!(val instanceof Buffer
        || val instanceof Date
        || val instanceof RegExp);
}
function cloneSpecificValue(val) {
    if (val instanceof Buffer) {
        var x = new Buffer(val.length);
        val.copy(x);
        return x;
    }
    else if (val instanceof Date) {
        return new Date(val.getTime());
    }
    else if (val instanceof RegExp) {
        return new RegExp(val);
    }
    else {
        throw new Error('Unexpected situation');
    }
}
