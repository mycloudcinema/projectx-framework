/*
 ____________                                           _
 |___  /  ____|                                        | |
   / /| |__ _ __ __ _ _ __ ___   _____      _____  _ __| | __
  / / |  __| '__/ _` | '_ ` _ \ / _ \ \ /\ / / _ \| '__| |/ /
 / /__| |  | | | (_| | | | | | |  __/\ V  V / (_) | |  |   <
/_____|_|  |_|  \__,_|_| |_| |_|\___| \_/\_/ \___/|_|  |_|\_\

*/

///<reference path='../tsc-descriptors/node/node.d.ts'/>
///<reference path='../tsc-descriptors/mysql.d.ts'/>

import * as fs from 'fs';
import * as path from 'path';
import {getLogger} from "../external/Logger";

const logger = getLogger("Settings Manager", self);
var self = this;

const SETTINGS_DIRECTORY_PATH = "../settings";
const HOSTNAME = require('os').hostname();

export interface ISettings {
	port?:number;
	template?:ITemplateSettings;
	cache?:ICacheSettings;
	dictionary?:mysql.IConnectionOptions;
	webservice?:mysql.IConnectionOptions;
	security?:mysql.IConnectionOptions;
	resource?:IResourceSettings;
	session?:ISessionSettings;
	app?:IAppSettings;
	faye?:IFayeSettings;
	linked_sites?:[string];
}

interface IFayeSettings {
	server?:string;
	server_internal?:string;
}
interface IResourceSettings {
	mediaFolderPath?:string;
}
interface ITemplateSettings {
	compressResources?:boolean;
	defaultTemplateName?:string;
	defaultBaseTemplateName?:string;
	assetPath?:string;
	defaultLanguage?:string;
	supportedLanguages?:[string];
	notFoundTemplatePath?:string;
}
interface ICacheSettings {
	minifyHtml?:boolean;
	javascript?:IJavascriptCacheSettings
	minifyCss?:boolean;
	defaultCacheInterval?:ICacheInterval;
}
interface ICacheInterval {
	weeks?:number;
	days?:number;
	hours?:number;
	minutes?:number;
	seconds?:number;
}
interface ISessionSettings {
	redirectIfMissing?: any[],
	expires?:number;
	nonSecureItems?:string[];
	loginConnection?:mysql.IConnectionOptions;
	sessionStoreConnection?:mysql.IConnectionOptions;
	rememberCookieLength?:number;
}

interface IJavascriptCacheSettings {
	sequences?:boolean      // join consecutive statemets with the “comma operator”     true
	properties?:boolean     // optimize property access: a["foo"] → a.foo               true
	dead_code?:boolean      // discard unreachable code                                 true
	drop_debugger?:boolean  // discard “debugger” statements                            true
	unsafe?:boolean         // some unsafe optimizations (see below)                    false
	conditionals?:boolean   // optimize if-s and conditional expressions                true
	comparisons?:boolean    // optimize comparisons                                     true
	evaluate?:boolean       // evaluate constant expressions                            true
	booleans?:boolean       // optimize boolean expressions                             true
	loops?:boolean          // optimize loops                                           true
	unused?:boolean         // drop unused variables/functions                          true
	hoist_funs?:boolean     // hoist function declarations                              true
	hoist_vars?:boolean     // hoist variable declarations                              false
	if_return?:boolean      // optimize if-s followed by return/continue                true
	join_vars?:boolean      // join var declarations                                    true
	cascade?:boolean        // try to cascade `right` into `left` in sequences          true
	side_effects?:boolean   // drop side-effect-free statements                         true
	warnings?:boolean       // warn about potentially dangerous optimizations/code      true
}
interface IAppSettings {
	site_id?:number
	site_name?:string;
	log_level?:string;
	port_number?:number;
	public_website?:boolean;
	db_timeout?:number;
}

global.template = {
	"defaultBaseTemplateName": null,
	"assetPath":               null,
	"defaultLanguage":         null,
	"supportedLanguages":      null
};

//export var settings = null;

let watchingDefaultSettingsFile = false;
let watchingLocalSettingsFile = false;

function loadSettingsFiles() {

	let defaultSettings:any;
	let localSettings:any;

	const defaultSettingsFile = path.join(process.cwd(), SETTINGS_DIRECTORY_PATH, '/default.json');
	const localSettingsFile = locateCustomSettingsFile();

	try {
		defaultSettings = JSON.parse(fs.readFileSync(defaultSettingsFile, 'utf8'));
	} catch (error) {
		logger.error(`Could not locate default settings file "${defaultSettingsFile}" or the file is not valid JSON`, error);
	}

	try {
		localSettings = JSON.parse(fs.readFileSync(localSettingsFile, 'utf8'));
	} catch (error) {
		// This is a normal error as a local or customer settings override is not required.
	}

	if (typeof defaultSettings !== 'undefined') {

		if (!watchingDefaultSettingsFile) {
			watchingDefaultSettingsFile = true;
			fs.watchFile(defaultSettingsFile, (current, previous) => {
				logger.info("Detected a change in settings.json. Reloading settings...");
				loadSettingsFiles();
			});
		}

		if (typeof localSettings !== 'undefined') {
			defaultSettings = deepExtend(defaultSettings, localSettings);
			logger.info(`Extending default settings with settings for ${HOSTNAME}`);
			if (!watchingLocalSettingsFile) {
				watchingLocalSettingsFile = true;
				fs.watchFile(localSettingsFile, (current, previous) => {
					logger.info("Detected a change in settings.json. Reloading settings...");
					loadSettingsFiles();
				});
			}
		}

		// logger.info(`localSettings: ${JSON.stringify(localSettings)}`);
		// logger.info(`defaultSettings: ${JSON.stringify(defaultSettings)}`);

		for (var key in defaultSettings) {
			self[key] = defaultSettings[key];
		}
		logger.info('Settings module loaded');
	}

}

function locateCustomSettingsFile() {

	const customSettingsFile = process.argv[2];
	const localSettingsFile = path.join(process.cwd(), SETTINGS_DIRECTORY_PATH, "/" + HOSTNAME + ".json");

	// logger.info(`Custom settings file: ${customSettingsFile}`);
	// logger.info(`Local settings file: ${localSettingsFile}`);

	if (customSettingsFile && fs.existsSync(customSettingsFile)) {
		logger.info(`Using custom settings file: ${customSettingsFile}.`);
		return customSettingsFile;
	} else if (fs.existsSync(localSettingsFile)) {
		logger.info(`Using local settings file: ${localSettingsFile}.`);
		return localSettingsFile;
	} else {
		if (customSettingsFile) {
			logger.warn(`Neither local settings file '${localSettingsFile}' nor custom settings file '${customSettingsFile}' were found.`);
		} else {
			logger.warn(`Local settings file '${localSettingsFile}' was not found.`);
		}
		return undefined;
	}
}

{
	loadSettingsFiles();
}

///// EXTEND
/**
 * Extening object that entered in first argument.
 *
 * Returns extended object or false if have no target object or incorrect type.
 *
 * If you wish to clone source object (without modify it), just use empty new
 * object as first argument, like this:
 *   deepExtend({}, yourObj_1, [yourObj_N]);
 */
function deepExtend(...args:any[]) {
	// convert arguments to array and cut off target object
	var target = args[0];
	var val, src, clone;

	args.forEach(function (obj) {
		// skip argument if it is array or isn't object
		if (typeof obj !== 'object' || Array.isArray(obj)) {
			return;
		}

		Object.keys(obj).forEach(function (key) {
			src = target[key]; // source value
			val = obj[key]; // new value

			// recursion prevention
			if (val === target) {
				return;

				/**
				 * if new value isn't object then just overwrite by new value
				 * instead of extending.
				 */
			} else if (typeof val !== 'object' || val === null) {
				target[key] = val;
				return;

				// just clone arrays (and recursive clone objects inside)
			} else if (Array.isArray(val)) {
				target[key] = deepCloneArray(val);
				return;

				// custom cloning and overwrite for specific objects
			} else if (isSpecificValue(val)) {
				target[key] = cloneSpecificValue(val);
				return;

				// overwrite by new value if source isn't object or array
			} else if (typeof src !== 'object' || src === null || Array.isArray(src)) {
				target[key] = deepExtend({}, val);
				return;

				// source value and new value is objects both, extending...
			} else {
				target[key] = deepExtend(src, val);
				return;
			}
		});
	});

	return target;
}

/**
 * Recursive cloning array.
 */
function deepCloneArray(arr) {
	var clone = [];
	arr.forEach(function (item, index) {
		if (typeof item === 'object' && item !== null) {
			if (Array.isArray(item)) {
				clone[index] = deepCloneArray(item);
			} else if (isSpecificValue(item)) {
				clone[index] = cloneSpecificValue(item);
			} else {
				clone[index] = deepExtend({}, item);
			}
		} else {
			clone[index] = item;
		}
	});
	return clone;
}

// Functions for extending an object:
function isSpecificValue(val) {
	return !!(
		val instanceof Buffer
		|| val instanceof Date
		|| val instanceof RegExp
	);
}

function cloneSpecificValue(val):any {
	if (val instanceof Buffer) {
		var x = new Buffer(val.length);
		val.copy(x);
		return x;
	} else if (val instanceof Date) {
		return new Date(val.getTime());
	} else if (val instanceof RegExp) {
		return new RegExp(val);
	} else {
		throw new Error('Unexpected situation');
	}
}
