"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var path = require("path");
var pug = require("pug");
var TemplateManager_1 = require("./TemplateManager");
var EmailTemplateManager = (function () {
    function EmailTemplateManager(basePath, dictionaryData) {
        if (dictionaryData !== null) {
            this.dictionaryData = dictionaryData;
        }
        this.templatesDirectory = basePath;
    }
    EmailTemplateManager.prototype.getDictionary = function (language) {
        return 'asd';
    };
    EmailTemplateManager.prototype.getTemplate = function (templatePath, language, compress, data) {
        var source = '';
        source = pug.renderFile(path.join(this.templatesDirectory, templatePath), Object.assign({}, data, {
            dictionary: {
                CANCEL: 'Cancel'
            }
        }));
        var template = new TemplateManager_1.Template(language, source, compress);
        return template;
    };
    return EmailTemplateManager;
}());
exports.EmailTemplateManager = EmailTemplateManager;
