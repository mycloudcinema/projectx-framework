"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Template = (function () {
    function Template(language, source, compressed) {
        this._language = language;
        this._source = source;
        this._compressed = compressed;
        this.size = source.length;
    }
    Object.defineProperty(Template.prototype, "language", {
        get: function () {
            return this._language;
        },
        set: function (value) {
            this._language = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Template.prototype, "source", {
        get: function () {
            return this._source;
        },
        set: function (value) {
            this._source = value;
            this.size = this._source.length;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Template.prototype, "compressed", {
        get: function () {
            return this._compressed;
        },
        set: function (value) {
            this._compressed = value;
        },
        enumerable: true,
        configurable: true
    });
    return Template;
}());
exports.Template = Template;
