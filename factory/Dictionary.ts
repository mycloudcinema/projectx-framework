/*
 ____________                                           _
 |___  /  ____|                                        | |
   / /| |__ _ __ __ _ _ __ ___   _____      _____  _ __| | __
  / / |  __| '__/ _` | '_ ` _ \ / _ \ \ /\ / / _ \| '__| |/ /
 / /__| |  | | | (_| | | | | | |  __/\ V  V / (_) | |  |   <
/_____|_|  |_|  \__,_|_| |_| |_|\___| \_/\_/ \___/|_|  |_|\_\

Author:		Forisz
Date:		06/04/15.

Dictionary functionality for the Framework application
*/

///<reference path='../tsc-descriptors/express.d.ts'/>
///<reference path='../tsc-descriptors/compression.d.ts'/>
///<reference path='../tsc-descriptors/mysql.d.ts'/>
///<reference path='../tsc-descriptors/node/node.d.ts'/>

import mysql = require("mysql");
import Utils = require("./Utils");
import Settings = require("../external/Settings");
import {getLogger} from '../external/Logger';
var settings:Settings.ISettings = Settings;

const logger = getLogger("Dictionary Manager", settings);

export var dictionaryPool = {};
export class DictionaryManager {
	private createDatabaseConnection():mysql.IConnection {
		logger.debug("Create dictionary database connection");
		// logger.debug(`Dictionary: ${settings.dictionary.connection.host}, ${settings.dictionary.connection.port}, ${settings.dictionary.connection.user}, ${settings.dictionary.connection.password}, ${settings.dictionary.connection.database}`);

		// If we haven't set the timeout values then we add them here.
		try {
			if (!settings.dictionary.connection.connectTimeout) {
				settings.dictionary.connection.connectTimeout = settings.app.db_timeout || 10000;
			}
			if (!settings.dictionary.connection.timeout) {
				settings.dictionary.connection.timeout = settings.app.db_timeout || 30000;
			}
		} catch (ex) {
			logger.error(ex);
		}

		var connection = mysql.createConnection(settings.dictionary.connection);
		connection.connect();
		logger.debug(`Dictionary connection established ${connection.config.host}`);
		return connection;
	}

	public loadDictionary = (callback?) => {
		for (var i = 0; i < settings.template.supportedLanguages.length; i++) {
			dictionaryPool[settings.template.supportedLanguages[i].toUpperCase()] = {}
		}
		var connection = this.createDatabaseConnection();
		logger.info(`Loading dictionary items for the following languages: ${settings.template.supportedLanguages}`);
		for (var i = 0; i < settings.template.supportedLanguages.length; i++) {
			// connection.query("CALL get_dictionary_items('" + settings.template.supportedLanguages[i].toUpperCase() + "', '" + settings.app.site_name + "')", (error, rows, fields) => {
			connection.query({
					sql:		"CALL get_dictionary_items(?,?)",
					timeout:	(settings.app.db_timeout || 30000),
				},
				[
					settings.template.supportedLanguages[i].toUpperCase(),
					settings.app.site_name
				],
				(error, rows, fields) => {
					if (error) {
						logger.error("There was an error while loading the dictionary items.", error);
						if (typeof callback === "function") {
							callback(true);
						}
					} else {
						var lang;
						for (var j = 0; j < rows[0].length; j++) {
							var item = rows[0][j];
							dictionaryPool[item.dictionary_language.toUpperCase()][item.dictionary_key] = item;
							lang = item.dictionary_language.toUpperCase();
							/*log("Dictionary item loaded: " + item.dictionary_key);*/
						}
						logger.debug(`${rows[0].length} dictionary items loaded for ${lang}`);
						if (lang === settings.template.supportedLanguages[settings.template.supportedLanguages.length - 1]) {
							logger.info("Dictionary items loaded. Closing connection...");
							connection.end();
							logger.info("Dictionary connection closed");
							if (typeof callback === "function") {
								callback(false);
							}
						}
					}
				}
			);
		}
	};
	public getDictionaryItem = (lang:string, key:string):any => {
		if (typeof lang === "string" && typeof key === "string")
			if (typeof dictionaryPool[lang.toUpperCase()] !== "undefined" && typeof dictionaryPool[lang.toUpperCase()][key.toUpperCase()] !== "undefined")
				return dictionaryPool[lang.toUpperCase()][key.toUpperCase()].dictionary_data;
		logger.warn(`Dictionary parse error. The requested item does not exist ${key}`);
		return null;
	};

	constructor() {

	}
}
