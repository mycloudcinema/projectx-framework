"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.walkDir = exports.WebservicesManager = exports.PageManager = void 0;
String.prototype.startsWith = function (str) {
    return this.indexOf(str) === 0;
};
var express = require("express");
var URL = require("url");
var Template = require("./Template");
var TemplateManager = Template.TemplateManager;
var mysql = require("mysql");
var Settings = require("../external/Settings");
var fwSession = require("./Session");
var Dictionary = require("./Dictionary");
var Logger_1 = require("../external/Logger");
var path = require("path");
var Session_1 = require("./Session");
var settings = Settings;
var logger_rm = Logger_1.getLogger("Request Manager", settings);
var logger_pm = Logger_1.getLogger("Page Manager  ", settings);
var logger_ws = Logger_1.getLogger("Webservice Manager", settings);
var pool = mysql.createPool(settings.webservice.connection);
pool.on("connection", function () {
    logger_rm.debug("Connection established from pool");
});
pool.on('enqueue', function () {
    logger_rm.debug('Waiting for available connection slot');
});
var Request = (function () {
    function Request() {
        this.sessionManager = new fwSession.SessionManager();
    }
    return Request;
}());
var PageManager = (function (_super) {
    __extends(PageManager, _super);
    function PageManager(asset_path) {
        var _this = _super.call(this) || this;
        _this.sessionManager = new fwSession.SessionManager();
        _this.doGet = function (request, response) {
            logger_pm.info("settings.app.public_website: " + settings.app.public_website);
            fwSession.validateRequest(request, response);
            var template_name = _this.getTemplateName(request.url);
            var template;
            if (template_name.path.startsWith("/public") || template_name.path.startsWith("/media")) {
                logger_pm.warn("Static resource " + template_name.path + " not found in public folders. Return 404 code.");
                response.status(404).end();
                return;
            }
            var userIsLoggedIn = _this.sessionManager.isLoggedIn(request);
            if (!settings.app.public_website && !userIsLoggedIn && settings.session.nonSecureItems.indexOf(template_name.path) === -1) {
                if (template_name.path !== "login")
                    request.session.redirect_to = template_name.path;
                response.redirect(Settings.template.loginTemplateName || "/base/login");
                return;
            }
            if (userIsLoggedIn && template_name.path === (Settings.template.loginTemplateName || "/base/login")) {
                response.redirect("/");
            }
            if (!settings.app.public_website && userIsLoggedIn && !Session_1.securityManager.hasAccess(request.session.user, template_name.path)) {
                response.redirect(Settings.template.noaccessTemplateName || "/base/noaccess");
                return;
            }
            try {
                if (fs.existsSync(process.cwd() + "/" + settings.template.assetPath + "/pagetemplates" + template_name.path + ".html")) {
                    logger_pm.info("Request resolved to template: " + template_name.path, request.session.user);
                    template = _this.templateManager.getTemplate(template_name.path, request.session.language, request.session);
                }
                else if (fs.existsSync(process.cwd() + "/" + settings.template.assetPath + "/pagetemplates" + template_name.path) &&
                    fs.lstatSync(process.cwd() + "/" + settings.template.assetPath + "/pagetemplates" + template_name.path).isDirectory()) {
                    if (fs.existsSync(process.cwd() + "/" + settings.template.assetPath + "/pagetemplates" + template_name.path + "/" + settings.template.defaultTemplateName + ".html")) {
                        logger_pm.info("Request resolved to template: " + template_name.path + settings.template.defaultTemplateName, request.session.user);
                        template = _this.templateManager.getTemplate(path.join(template_name.path, settings.template.defaultTemplateName), request.session.language, request.session);
                    }
                    else {
                        template = _this.templateManager.getTemplate(settings.template.notFoundTemplatePath, template_name.lang, request.session);
                    }
                }
                else {
                    logger_pm.warn(process.cwd() + "/" + settings.template.assetPath + "/pagetemplates" + template_name.path + ".html Does not exists", request.session.user);
                    template = _this.templateManager.getTemplate(settings.template.notFoundTemplatePath, template_name.lang, request.session);
                }
            }
            catch (e) {
                logger_pm.error("There was an error looking for template files. " + template_name.path, e, request.session.user);
                template = _this.templateManager.getTemplate("500", template_name.lang, request.session);
            }
            if (template !== null && typeof template !== "undefined") {
                response.send(template.source);
            }
            else {
                response.send("<h1>500</h1>");
            }
        };
        _this.doPost = function (request, response) {
        };
        _this.templateManager = new TemplateManager(asset_path);
        _this.router = express.Router();
        return _this;
    }
    PageManager.prototype.getTemplateName = function (url) {
        var parsedUrl = URL.parse(url);
        var regex = /^\/(\w\w)\/?$/;
        var match = parsedUrl.pathname.match(regex);
        var ret = {
            path: "",
            lang: ""
        };
        var match1 = parsedUrl.pathname.match(/^\/(\w{2})$/);
        var match2 = parsedUrl.pathname.match(/^\/(\w{2})\//);
        if (match1 !== null) {
            ret.path = "";
            ret.lang = match1[1].toUpperCase();
        }
        else if (match2 !== null) {
            ret.path = parsedUrl.pathname.replace("/" + match2[1], "");
            ret.lang = match2[1].toUpperCase();
        }
        else {
            ret.path = parsedUrl.pathname;
            ret.lang = settings.template.defaultLanguage;
        }
        return ret;
    };
    return PageManager;
}(Request));
exports.PageManager = PageManager;
var WebservicesManager = (function (_super) {
    __extends(WebservicesManager, _super);
    function WebservicesManager(rootPath) {
        var _this = _super.call(this) || this;
        _this.webservices = {};
        _this.sessionManager = new fwSession.SessionManager();
        _this.doGet = function (request, response) {
            var url = URL.parse(request.url, true);
            var matches = url.pathname.replace("/webservices/", "").split("/");
            var deviceId = _this.getDeviceId(request);
            var wsResponse;
            var userId = request.session.user ? request.session.user.user_id : 0;
            var userLanguage = request.session.language ? request.session.language : settings.template.defaultLanguage;
            logger_ws.debug("remek.device " + deviceId);
            logger_ws.debug("GET request received. " + request.url, request.session ? request.session.user : null);
            var webserviceName = matches.join("/");
            var functionName = matches.pop();
            var webservicePath = matches.join("/");
            logger_pm.info("settings.app.public_website: " + settings.app.public_website);
            if (!settings.app.public_website && !_this.sessionManager.isLoggedIn(request) && settings.session.nonSecureItems.indexOf(webserviceName) === -1) {
                response.json({ resultCode: 999, data: "User is not logged in." });
                return;
            }
            if (!settings.app.public_website && !Session_1.securityManager.hasAccess(request.session.user, webserviceName)) {
                response.json({ resultCode: 888, data: "You have no access to this function " + webserviceName });
                return;
            }
            switch (webserviceName) {
                case "dictionary/_reload":
                    var dictionaryManager = new Dictionary.DictionaryManager();
                    dictionaryManager.loadDictionary(function (error) {
                        if (error) {
                            response.json({ resultCode: 1, data: "Failed to reload dictionary" });
                        }
                        else {
                            response.json({ resultCode: 0, data: "Dictionary reloaded" });
                        }
                    });
                    return;
                case "security/reload":
                    Session_1.securityManager.reload();
                    response.json({ resultCode: 0, data: "Reloading security" });
                    return;
            }
            try {
                logger_ws.debug("Getting a connection from the pool...");
                pool.getConnection(function (error, connection) {
                    if (error !== null) {
                        logger_ws.error("Failed to retrieve connection from pool.", error);
                        wsResponse = {
                            data: "No DB connection",
                            resultCode: 500
                        };
                        connection.release();
                        logger_ws.debug("Connection released from pool - Exception - " + error);
                        response.json(wsResponse);
                    }
                    else {
                        logger_ws.debug("Connection established");
                        logger_ws.debug("Request query", JSON.stringify(url.query));
                        try {
                            _this.webservices[webservicePath][functionName](connection, function (error, rows, fields) {
                                connection.release();
                                logger_ws.debug("Connection released from pool");
                                if (error) {
                                    logger_ws.error("Database error", error, request.session.user);
                                    wsResponse = {
                                        data: (error.message || error.code),
                                        resultCode: (error.status || 2)
                                    };
                                }
                                else {
                                    logger_ws.debug("Database call success.");
                                    wsResponse = { data: rows[0], resultCode: 0 };
                                }
                                response.json(wsResponse);
                            }, {
                                device_id: deviceId,
                                ip_address: request.ip,
                                site_name: settings.app.site_name,
                                user_id: userId,
                                language: userLanguage,
                                query: url.query,
                                payload: request.body
                            }, request.session);
                        }
                        catch (e) {
                            if (connection) {
                                connection.release();
                                logger_ws.debug("Connection released from pool - Exception - " + e);
                            }
                            logger_ws.error("An unknown error occurred while executing the webservice call", e, request.session.user);
                            wsResponse = {
                                data: "Unsupported method called.",
                                resultCode: 3
                            };
                            response.json(wsResponse);
                        }
                    }
                });
            }
            catch (exception) {
                response.json({ data: "Webservice call (" + url.pathname + ") does not exist", resultCode: 1 });
                logger_ws.error("An unknown error occurred while executing the webservice call " + url.pathname, exception, request.session.user);
            }
        };
        _this.doPost = function (request, response) {
            var url = URL.parse(request.url, true);
            var matches = url.pathname.replace("/webservices/", "").split("/");
            var deviceId = _this.getDeviceId(request);
            var wsResponse;
            logger_ws.debug("remek.device " + deviceId);
            logger_ws.debug("POST request received. " + request.url, request.session ? request.session.user : null);
            var webserviceName = matches.join("/");
            var functionName = matches.pop();
            var webservicePath = matches.join("/");
            var userId = request.session.user ? request.session.user.user_id : 0;
            var userLanguage = request.session.language ? request.session.language : settings.template.defaultLanguage;
            logger_pm.info("settings.app.public_website: " + settings.app.public_website);
            if (!settings.app.public_website && !_this.sessionManager.isLoggedIn(request) && settings.session.nonSecureItems.indexOf(webserviceName) === -1) {
                response.json({ resultCode: 999, data: "User is not logged in." });
                return;
            }
            if (!settings.app.public_website && !Session_1.securityManager.hasAccess(request.session.user, webserviceName)) {
                response.json({ resultCode: 888, data: "You have no access to this function " + webserviceName });
                return;
            }
            try {
                logger_ws.debug("Getting a connection from the pool...");
                pool.getConnection(function (error, connection) {
                    if (error !== null) {
                        logger_ws.error("Failed to retrieve connection from pool.", error, request.session.user);
                        wsResponse = {
                            data: "No DB connection",
                            resultCode: 500
                        };
                        response.json(wsResponse);
                        connection.release();
                        logger_ws.debug("Connection released from pool - Exception - " + error);
                    }
                    else {
                        logger_ws.debug("Connection established", request.session.user);
                        try {
                            _this.webservices[webservicePath][functionName](connection, function (error, rows, fields) {
                                connection.release();
                                logger_ws.debug("Connection released from pool");
                                if (error) {
                                    logger_ws.error("Database error: ", error, request.session.user);
                                    wsResponse = {
                                        data: (error.message || error.code),
                                        resultCode: (error.status || 2)
                                    };
                                }
                                else {
                                    logger_ws.debug("Database call success.", request.session.user);
                                    wsResponse = { data: rows[0], resultCode: 0 };
                                }
                                response.json(wsResponse);
                            }, {
                                device_id: deviceId,
                                ip_address: request.ip,
                                site_name: settings.app.site_name,
                                user_id: userId,
                                language: userLanguage,
                                query: url.query,
                                payload: request.body
                            }, request.session);
                        }
                        catch (e) {
                            if (connection) {
                                connection.release();
                                logger_ws.debug("Connection released from pool - Exception - " + e);
                            }
                            logger_ws.error("An unknown error occurred while executing the webservice call", e, request.session.user);
                            wsResponse = {
                                data: "Unsupported method called.",
                                resultCode: 500
                            };
                            response.json(wsResponse);
                        }
                    }
                });
            }
            catch (exception) {
                response.json({ data: "Webservice call (" + url.pathname + ") does not exist", resultCode: 1 });
                logger_ws.error("An unknown error occurred while executing the webservice call", exception, request.session.user);
            }
        };
        var ws = _this.webservices;
        var normalizedPath = require("path").join("webservices");
        logger_ws.info("Loading webservice classes");
        _this.loadWebservices();
        require("fs").readdirSync(process.cwd() + "/../webservices").forEach(function (file) {
            if (file.split(".")[file.split(".").length - 1] === "js") {
                _this.webservices[file.split(".")[0]] = require(process.cwd() + "/../webservices/" + file);
                logger_ws.debug("Webservice loaded: " + file);
            }
        });
        process.on("exit", function (exitCode) {
        });
        return _this;
    }
    WebservicesManager.prototype.loadWebservices = function () {
        var tmp = walkDir(process.cwd() + "/../webservices/", null);
        for (var i = 0; i < tmp.length; i++) {
            var obj = tmp[i];
            this.webservices[obj.name] = obj.module;
        }
    };
    WebservicesManager.prototype.parseCookies = function (request) {
        var list = {}, rc = request.headers.cookie;
        rc && rc.split(';').forEach(function (cookie) {
            var parts = cookie.split('=');
            list[parts.shift().trim()] = decodeURI(parts.join('='));
        });
        return list;
    };
    WebservicesManager.prototype.getDeviceId = function (request) {
        var cookies = this.parseCookies(request);
        return cookies['remek.device'];
    };
    return WebservicesManager;
}(Request));
exports.WebservicesManager = WebservicesManager;
var fs = require("fs");
function walkDir(dir, filelist) {
    var files = fs.readdirSync(dir);
    filelist = filelist || [];
    files.forEach(function (file) {
        if (fs.statSync(dir + file).isDirectory()) {
            filelist = walkDir(dir + file + '/', filelist);
        }
        else {
            var the_file = dir + file;
            if (the_file.indexOf(".js", the_file.length - 3) !== -1) {
                var obj = {
                    name: the_file.substring(0, the_file.length - 3).split("/../webservices/")[1],
                    module: require(the_file)
                };
                filelist.push(obj);
            }
        }
    });
    return filelist;
}
exports.walkDir = walkDir;
