/*
 ____________                                           _
 |___  /  ____|                                        | |
   / /| |__ _ __ __ _ _ __ ___   _____      _____  _ __| | __
  / / |  __| '__/ _` | '_ ` _ \ / _ \ \ /\ / / _ \| '__| |/ /
 / /__| |  | | | (_| | | | | | |  __/\ V  V / (_) | |  |   <
/_____|_|  |_|  \__,_|_| |_| |_|\___| \_/\_/ \___/|_|  |_|\_\

Author:		Forisz
Date:		06/04/15.

Handles all requests through the Framework application
*/

String.prototype.startsWith = function (str) {
	return this.indexOf(str) === 0;
};

///<reference path='../tsc-descriptors/express.d.ts'/>
///<reference path='../tsc-descriptors/express-session.d.ts'/>
///<reference path='../tsc-descriptors/node/node.d.ts'/>
///<reference path='../tsc-descriptors/mysql.d.ts'/>

import express = require("express");
import URL = require("url");
import Utils = require("./Utils");
import Template = require("./Template");
import TemplateManager = Template.TemplateManager;
import mysql = require("mysql");
import http = require("http");
import Security = require("./Security");
import Settings = require("../external/Settings");
import fs = require("fs");
import fwSession = require("./Session");
import Dictionary = require("./Dictionary");
import {getLogger} from "../external/Logger";
import * as path from "path";

import {securityManager} from "./Session";

var settings:Settings.ISettings = Settings;

const logger_rm = getLogger("Request Manager", settings);
const logger_pm = getLogger("Page Manager  ", settings);
const logger_ws = getLogger("Webservice Manager", settings);

var pool = mysql.createPool(settings.webservice.connection);
pool.on("connection", function () {
	logger_rm.debug("Connection established from pool");
});
pool.on('enqueue', function () {
	logger_rm.debug('Waiting for available connection slot');
});

interface IRequestHandler {
	doGet(request:express.Request, response:express.Response):void;
	doPost(request:express.Request, response:express.Response):void;
}

class Request {
	protected sessionManager;

	constructor() {
		this.sessionManager = new fwSession.SessionManager();
	}
}

export class PageManager extends Request implements IRequestHandler {

	templateManager:TemplateManager;
	router:express.Router;
	sessionManager = new fwSession.SessionManager();

	public doGet = (request:express.Request, response:express.Response):void => {

		logger_pm.info(`settings.app.public_website: ${settings.app.public_website}`);
		fwSession.validateRequest(request, response);

		var template_name = this.getTemplateName(request.url);
		var template;

		//TODO: Remove hard-code public folder definitions
		if (template_name.path.startsWith("/public") || template_name.path.startsWith("/media")) {
			logger_pm.warn(`Static resource ${template_name.path} not found in public folders. Return 404 code.`);
			response.status(404).end();
			return;
		}

		var userIsLoggedIn = this.sessionManager.isLoggedIn(request);
		if (!settings.app.public_website && !userIsLoggedIn && settings.session.nonSecureItems.indexOf(template_name.path) === -1) {
			if (template_name.path !== "login")
				request.session.redirect_to = template_name.path;
			response.redirect(Settings.template.loginTemplateName || "/base/login");
			return;
		}
		// If the user is logged in and requested the login template, then redirect to the root path
		if (userIsLoggedIn && template_name.path === (Settings.template.loginTemplateName || "/base/login")) {
			response.redirect("/");
		}
		if (!settings.app.public_website && userIsLoggedIn && !securityManager.hasAccess(request.session.user, template_name.path)) {
			response.redirect(Settings.template.noaccessTemplateName || "/base/noaccess");
			return;
		}

		try {

			if (fs.existsSync(process.cwd() + "/" + settings.template.assetPath + "/pagetemplates" + template_name.path + ".html")) {

				// If the file exists then parse it through the TemplateManager
				logger_pm.info(`Request resolved to template: ${template_name.path}`, request.session.user);
				template = this.templateManager.getTemplate(template_name.path, request.session.language, request.session);

			} else if (fs.existsSync(process.cwd() + "/" + settings.template.assetPath + "/pagetemplates" + template_name.path) &&

				fs.lstatSync(process.cwd() + "/" + settings.template.assetPath + "/pagetemplates" + template_name.path).isDirectory()) {

				if (fs.existsSync(process.cwd() + "/" + settings.template.assetPath + "/pagetemplates" + template_name.path + "/" + settings.template.defaultTemplateName + ".html")) {
					// if (fs.existsSync(process.cwd() + "/" + settings.template.assetPath + "/pagetemplates" + template_name.path + ".html")) {
					logger_pm.info(`Request resolved to template: ${template_name.path}` + settings.template.defaultTemplateName, request.session.user);
					template = this.templateManager.getTemplate(path.join(template_name.path, settings.template.defaultTemplateName), request.session.language, request.session);
				} else {
					template = this.templateManager.getTemplate(settings.template.notFoundTemplatePath, template_name.lang, request.session);
				}

			} else {
				logger_pm.warn(process.cwd() + "/" + settings.template.assetPath + "/pagetemplates" + template_name.path + ".html Does not exists", request.session.user);
				template = this.templateManager.getTemplate(settings.template.notFoundTemplatePath, template_name.lang, request.session);
			}

		} catch (e) {
			logger_pm.error(`There was an error looking for template files. ${template_name.path}`, e, request.session.user);
			template = this.templateManager.getTemplate("500", template_name.lang, request.session);
		}

		if (template !== null && typeof template !== "undefined") {
			response.send(template.source);
		} else {
			response.send("<h1>500</h1>");
		}

	};

	public doPost = (request:express.Request, response:express.Response):void => {
		// TODO
	};

	private getTemplateName(url) {

		var parsedUrl = URL.parse(url);
		var regex = /^\/(\w\w)\/?$/;
		var match = parsedUrl.pathname.match(regex);
		var ret = {
			path: "",
			lang: ""
		};

		var match1 = parsedUrl.pathname.match(/^\/(\w{2})$/);
		var match2 = parsedUrl.pathname.match(/^\/(\w{2})\//);

		if (match1 !== null) {
			ret.path = "";
			ret.lang = match1[1].toUpperCase();
		} else if (match2 !== null) {
			ret.path = parsedUrl.pathname.replace("/" + match2[1], "");
			ret.lang = match2[1].toUpperCase();
		} else {
			ret.path = parsedUrl.pathname;
			ret.lang = settings.template.defaultLanguage;
			// ret.lang = "EN";
		}

		return ret

	}

	constructor(asset_path:string) {
		super();
		this.templateManager = new TemplateManager(asset_path);
		this.router = express.Router();
	}
}

export class WebservicesManager extends Request implements IRequestHandler {

	private webservices = {};
	sessionManager = new fwSession.SessionManager();

	doGet = (request:express.Request, response:express.Response):void => {

		var url = URL.parse(request.url, true);
		var matches = url.pathname.replace("/webservices/", "").split("/");
		var deviceId = this.getDeviceId(request);
		var wsResponse;
		var userId = request.session.user ? request.session.user.user_id : 0;
		var userLanguage = request.session.language ? request.session.language : settings.template.defaultLanguage;

		logger_ws.debug(`remek.device ${deviceId}`);
		logger_ws.debug(`GET request received. ${request.url}`, request.session ? request.session.user : null);

		// The full webservice signature for security check
		var webserviceName = matches.join("/");
		// Name of the function to call inside the module.
		var functionName = matches.pop();
		// Path to the webservice file. (also the key for the file in this.webservices)
		var webservicePath = matches.join("/");

		// Login and security checks
		logger_pm.info(`settings.app.public_website: ${settings.app.public_website}`);
		if (!settings.app.public_website && !this.sessionManager.isLoggedIn(request) && settings.session.nonSecureItems.indexOf(webserviceName) === -1) {
			response.json({resultCode: 999, data: "User is not logged in."});
			return;
		}
		if (!settings.app.public_website && !securityManager.hasAccess(request.session.user, webserviceName)) {
			response.json({resultCode: 888, data: "You have no access to this function " + webserviceName});
			return;
		}

		switch (webserviceName) {
			case "dictionary/_reload":
				var dictionaryManager = new Dictionary.DictionaryManager();
				dictionaryManager.loadDictionary((error)=> {
					if (error) {
						response.json({resultCode: 1, data: "Failed to reload dictionary"});
					} else {
						response.json({resultCode: 0, data: "Dictionary reloaded"});
					}
				});
				return;

			case "security/reload":
				securityManager.reload();
				response.json({ resultCode: 0, data: "Reloading security" });
				return;
		}

		try {
			logger_ws.debug("Getting a connection from the pool...");
			pool.getConnection((error, connection) => {

				if (error !== null) {
					logger_ws.error("Failed to retrieve connection from pool.", error);
					wsResponse = {
						data:       "No DB connection",
						resultCode: 500
					};
					connection.release();
					logger_ws.debug(`Connection released from pool - Exception - ${error}`);
					response.json(wsResponse);
				} else {

					logger_ws.debug("Connection established");
					logger_ws.debug("Request query", JSON.stringify(url.query));

					try {

						this.webservices[webservicePath][functionName](connection, function (error, rows, fields) {

							connection.release();
							logger_ws.debug("Connection released from pool");

							if (error) {
								// There was a database error -->
								//                                console.log("Database error: ", error);
								logger_ws.error("Database error", error, request.session.user);
								wsResponse = {
									data:       (error.message || error.code),
									resultCode: (error.status || 2)
								}
							} else {
								logger_ws.debug("Database call success.");
								wsResponse = {data: rows[0], resultCode: 0}
							}
							response.json(wsResponse);

						}, {
							device_id:  		deviceId,
							ip_address:			request.ip,
							site_name:			settings.app.site_name,
							user_id:			userId,
							language:			userLanguage,
							query:				url.query,
							payload:			request.body
						}, request.session);

					} catch (e) {
						if (connection) {
							connection.release();
							logger_ws.debug(`Connection released from pool - Exception - ${e}`);
						}
						logger_ws.error("An unknown error occurred while executing the webservice call", e, request.session.user);
						wsResponse = {
							data:       "Unsupported method called.",
							resultCode: 3
						};
						response.json(wsResponse);
					}
				}
			});
		} catch (exception) {
			response.json({data: "Webservice call (" + url.pathname + ") does not exist", resultCode: 1});
			logger_ws.error(`An unknown error occurred while executing the webservice call ${url.pathname}`, exception, request.session.user);
		}
		//response.json(wsResponse)
	};

	doPost = (request:express.Request, response:express.Response):void => {

		var url = URL.parse(request.url, true);
		var matches = url.pathname.replace("/webservices/", "").split("/");
		var deviceId = this.getDeviceId(request);
		var wsResponse;

		logger_ws.debug(`remek.device ${deviceId}`);
		logger_ws.debug(`POST request received. ${request.url}`, request.session ? request.session.user : null);

		// The full webservice signature for security check
		var webserviceName = matches.join("/");
		// Name of the function to call inside the module.
		var functionName = matches.pop();
		// Path to the webservice file. (also the key for the file in this.webservices)
		var webservicePath = matches.join("/");

		var userId = request.session.user ? request.session.user.user_id : 0;
		var userLanguage = request.session.language ? request.session.language : settings.template.defaultLanguage;

		// Login and security checks
		logger_pm.info(`settings.app.public_website: ${settings.app.public_website}`);
		if (!settings.app.public_website && !this.sessionManager.isLoggedIn(request) && settings.session.nonSecureItems.indexOf(webserviceName) === -1) {
			response.json({resultCode: 999, data: "User is not logged in."});
			return;
		}
		if (!settings.app.public_website && !securityManager.hasAccess(request.session.user, webserviceName)) {
			response.json({resultCode: 888, data: "You have no access to this function " + webserviceName});
			return;
		}

		try {
			logger_ws.debug("Getting a connection from the pool...");
			pool.getConnection((error, connection) => {
				if (error !== null) {
					logger_ws.error("Failed to retrieve connection from pool.", error, request.session.user);
					wsResponse = {
						data:       "No DB connection",
						resultCode: 500
					};
					response.json(wsResponse);
					connection.release();
					logger_ws.debug(`Connection released from pool - Exception - ${error}`);
				} else {
					logger_ws.debug("Connection established", request.session.user);
					try {

						this.webservices[webservicePath][functionName](connection, function (error, rows, fields) {

							connection.release();
							logger_ws.debug("Connection released from pool");

							if (error) {
								// There was a database error -->
								logger_ws.error("Database error: ", error, request.session.user);
								wsResponse = {
									data:       (error.message || error.code),
									resultCode: (error.status || 2)
								}
							} else {
								logger_ws.debug("Database call success.", request.session.user);
								wsResponse = {data: rows[0], resultCode: 0}
							}

							response.json(wsResponse);

						}, {
							device_id:  		deviceId,
							ip_address:			request.ip,
							site_name:			settings.app.site_name,
							user_id:			userId,
							language:			userLanguage,
							query:				url.query,
							payload:			request.body
						}, request.session);

					} catch (e) {
						if (connection) {
							connection.release();
							logger_ws.debug(`Connection released from pool - Exception - ${e}`);
						}
						logger_ws.error("An unknown error occurred while executing the webservice call", e, request.session.user);
						wsResponse = {
							data:       "Unsupported method called.",
							resultCode: 500
						};
						response.json(wsResponse);
					}
				}
			});
		} catch (exception) {
			response.json({data: "Webservice call (" + url.pathname + ") does not exist", resultCode: 1});
			logger_ws.error("An unknown error occurred while executing the webservice call", exception, request.session.user);
		}
	};

	private loadWebservices():void {
		var tmp = walkDir(process.cwd() + "/../webservices/", null);
		for (var i = 0; i < tmp.length; i++) {
			var obj = tmp[i];
			this.webservices[obj.name] = obj.module;
		}
	}

	private parseCookies(request:any):any {

		var list = {},
		rc = request.headers.cookie;

		rc && rc.split(';').forEach(function( cookie ) {
			var parts = cookie.split('=');
			list[parts.shift().trim()] = decodeURI(parts.join('='));
		});

		return list;
	}

	private getDeviceId(request:any):string {
		var cookies = this.parseCookies(request);
		return cookies['remek.device'];
	}

	constructor(rootPath) {

		super();

		var ws = this.webservices;
		var normalizedPath = require("path").join("webservices");
		//log_ws(rootPath +"/"+normalizedPath);
		logger_ws.info("Loading webservice classes");

		this.loadWebservices();

		require("fs").readdirSync(process.cwd() + "/../webservices").forEach((file) => {
			if (file.split(".")[file.split(".").length - 1] === "js") {
				this.webservices[file.split(".")[0]] = require(process.cwd() + "/../webservices/" + file);
				logger_ws.debug("Webservice loaded: " + file);
			}
		});

		process.on("exit", (exitCode) => {
			//pool.end();
		});

	}

}

var fs = require("fs");

export function walkDir(dir, filelist) {
	var files = fs.readdirSync(dir);
	filelist = filelist || [];
	files.forEach((file:String) => {
		if (fs.statSync(dir + file).isDirectory()) {
			filelist = walkDir(dir + file + '/', filelist);
		}
		else {
			var the_file = dir + file;
			if (the_file.indexOf(".js", the_file.length - 3) !== -1) {
				var obj = {
					name:   the_file.substring(0, the_file.length - 3).split("/../webservices/")[1],
					module: require(the_file)
				};
				filelist.push(obj);
			}
		}
	});
	return filelist;
}
