"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getResourceFilePath = exports.ResourceFileType = void 0;
var fs = require("fs-extra");
var settings = require("../external/Settings");
var Logger_1 = require("../external/Logger");
var logger = Logger_1.getLogger("Resources Manager", settings);
var HOSTNAME = require('os').hostname();
var resourcesData = new Map();
var ResourceFileType;
(function (ResourceFileType) {
    ResourceFileType[ResourceFileType["SCRIPT"] = 0] = "SCRIPT";
    ResourceFileType[ResourceFileType["STYLE"] = 1] = "STYLE";
    ResourceFileType[ResourceFileType["USER_DEFINED"] = 2] = "USER_DEFINED";
})(ResourceFileType = exports.ResourceFileType || (exports.ResourceFileType = {}));
var ResourceFile = (function () {
    function ResourceFile(data) {
        var _this = this;
        this._src = (function (src) {
            if (typeof src === 'string') {
                return src;
            }
            throw new Error('Invalid resource json file. Missing or wrong field [src]');
        })(data.src);
        this._id = (function (id) {
            if (typeof id === 'string') {
                return id;
            }
            logger.warn("Resource file (" + _this._src + ") has no resource identifier. To be able to use the resource with $resource{} parsing you have to supply a an id");
            return null;
        })(data.id);
        this._type = (function (type) {
            if (typeof type === 'string') {
                switch (type.toLowerCase()) {
                    case "script":
                        return ResourceFileType.SCRIPT;
                    case "style":
                        return ResourceFileType.STYLE;
                    default:
                        return ResourceFileType.USER_DEFINED;
                }
            }
        })(data.type);
    }
    Object.defineProperty(ResourceFile.prototype, "src", {
        get: function () {
            return this._src;
        },
        set: function (v) {
            this._src = v;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ResourceFile.prototype, "type", {
        get: function () {
            return this._type;
        },
        set: function (v) {
            this._type = v;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ResourceFile.prototype, "name", {
        get: function () {
            return this._name;
        },
        set: function (value) {
            this._name = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ResourceFile.prototype, "id", {
        get: function () {
            return this._id;
        },
        set: function (value) {
            this._id = value;
        },
        enumerable: false,
        configurable: true
    });
    return ResourceFile;
}());
var Resource = (function () {
    function Resource(data) {
        this._dependencies = (function (dependencies) {
            if (Array.isArray(dependencies)) {
                return dependencies;
            }
            return [];
        })(data.dependencies);
        this._files = (function (files) {
            if (Array.isArray(files)) {
                return files.map(function (file) { return new ResourceFile(file); });
            }
            throw new Error('Invalid resource file. The resource file list (files) property is not an array.');
        })(data.files);
    }
    Object.defineProperty(Resource.prototype, "name", {
        get: function () {
            return this._name;
        },
        set: function (value) {
            this._name = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Resource.prototype, "dependencies", {
        get: function () {
            return this._dependencies;
        },
        set: function (value) {
            this._dependencies = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Resource.prototype, "files", {
        get: function () {
            return this._files;
        },
        set: function (value) {
            this._files = value;
        },
        enumerable: false,
        configurable: true
    });
    return Resource;
}());
function loadResourceObject() {
    var defaultFile = parseResourceFile('../resources/default.json', null);
    if (defaultFile == null)
        throw new Error('There was an error loading the default resource file');
    var systemSpecificFile = fs.existsSync(process.argv[4]) ? parseResourceFile(process.argv[4], {}) : parseResourceFile("../resources/" + HOSTNAME + ".json", {});
    var combinedFile = deepExtend(defaultFile, systemSpecificFile);
    for (var key in combinedFile) {
        if (combinedFile.hasOwnProperty(key)) {
            resourcesData.set(key, new Resource(combinedFile[key]));
        }
    }
}
function parseResourceFile(filePath, defaultValue) {
    var tmp;
    try {
        logger.info("Loading resource file: " + filePath);
        tmp = JSON.parse(fs.readFileSync(filePath, 'utf8'));
    }
    catch (error) {
        logger.warn("Unable and unwilling to load resource files from file system: " + filePath);
        return defaultValue;
    }
    return tmp;
}
function init() {
    loadResourceObject();
}
{
    init();
}
function getResourceFilePath(resourceName) {
    var names = resourceName.split('.');
    if (Array.isArray(names) && names.length === 2) {
        try {
            return resourcesData.get(names[0]).files.find(function (file) { return file.id === names[1]; }).src;
        }
        catch (error) {
            logger.warn("Could not locate resource file with resource identifier " + resourceName);
            return null;
        }
    }
    return null;
}
exports.getResourceFilePath = getResourceFilePath;
function deepExtend() {
    var args = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        args[_i] = arguments[_i];
    }
    var target = args[0];
    var val, src, clone;
    args.forEach(function (obj) {
        if (typeof obj !== 'object' || Array.isArray(obj)) {
            return;
        }
        Object.keys(obj).forEach(function (key) {
            src = target[key];
            val = obj[key];
            if (val === target) {
                return;
            }
            else if (typeof val !== 'object' || val === null) {
                target[key] = val;
                return;
            }
            else if (Array.isArray(val)) {
                target[key] = deepCloneArray(val);
                return;
            }
            else if (isSpecificValue(val)) {
                target[key] = cloneSpecificValue(val);
                return;
            }
            else if (typeof src !== 'object' || src === null || Array.isArray(src)) {
                target[key] = deepExtend({}, val);
                return;
            }
            else {
                target[key] = deepExtend(src, val);
                return;
            }
        });
    });
    return target;
}
function deepCloneArray(arr) {
    var clone = [];
    arr.forEach(function (item, index) {
        if (typeof item === 'object' && item !== null) {
            if (Array.isArray(item)) {
                clone[index] = deepCloneArray(item);
            }
            else if (isSpecificValue(item)) {
                clone[index] = cloneSpecificValue(item);
            }
            else {
                clone[index] = deepExtend({}, item);
            }
        }
        else {
            clone[index] = item;
        }
    });
    return clone;
}
function isSpecificValue(val) {
    return !!(val instanceof Buffer
        || val instanceof Date
        || val instanceof RegExp);
}
function cloneSpecificValue(val) {
    if (val instanceof Buffer) {
        var x = new Buffer(val.length);
        val.copy(x);
        return x;
    }
    else if (val instanceof Date) {
        return new Date(val.getTime());
    }
    else if (val instanceof RegExp) {
        return new RegExp(val);
    }
    else {
        throw new Error('Unexpected situation');
    }
}
