/*
 ____________                                           _
 |___  /  ____|                                        | |
   / /| |__ _ __ __ _ _ __ ___   _____      _____  _ __| | __
  / / |  __| '__/ _` | '_ ` _ \ / _ \ \ /\ / / _ \| '__| |/ /
 / /__| |  | | | (_| | | | | | |  __/\ V  V / (_) | |  |   <
/_____|_|  |_|  \__,_|_| |_| |_|\___| \_/\_/ \___/|_|  |_|\_\

Author:		Forisz
Date:		06/04/15.

Resource management
*/

import * as fs from 'fs-extra';
import * as path from 'path';
import * as settings from '../external/Settings';
import {getLogger} from "../external/Logger";
import * as URL from "url";
import * as http from "http";

const logger = getLogger("Resources Manager", settings);
/**
 * The name of the current host. The module will look for files with the following name: <hostname>.json
 * @type {any|string}
 */
const HOSTNAME = require('os').hostname();

let resourcesData:Map<string, Resource> = new Map();

export enum ResourceFileType {
	/**
	 * JavaScript file. Results in a <script> element
	 */
	SCRIPT,
	/**
	 * Cascading Style Sheet file. Results in a <link rel="stylesheet" /> element
	 */
	STYLE,
	/**
	 * User entered a custom type name
	 */
	USER_DEFINED
}

/**
 * Represents an actual file in the system.
 */
class ResourceFile {
	private _src:string;

	/**
	 * The source path of the file
	 * @returns {string}
	 */
	public get src() {
		return this._src
	}

	public set src(v) {
		this._src = v;
	}

	private _type:ResourceFileType;
	/**
	 * The type of the resource file
	 * @returns {ResourceFileType}
	 */
	public get type() {
		return this._type;
	}

	public set type(v) {
		this._type = v;
	}

	private _name: string;

	/**
	 * The name of the file without extensions
	 */
	get name():string {
		return this._name;
	}

	set name(value:string) {
		this._name = value;
	}

	/**
	 * Unique resource identifier
	 */
	private _id:string;

	get id():string {
		return this._id;
	}

	set id(value:string) {
		this._id = value;
	}



	/**
	 * Data must be a parsed JSON object from any of the resource json files.
	 * @param data
	 */
	constructor(data:any) {
		this._src = (src => {
			if (typeof src === 'string') {
				return src;
			}
			throw new Error('Invalid resource json file. Missing or wrong field [src]');
		})(data.src);

		this._id = ((id) => {
			if (typeof id === 'string') {
				return id;
			}
			logger.warn(`Resource file (${this._src}) has no resource identifier. To be able to use the resource with $resource{} parsing you have to supply a an id`);
			return null;
		})(data.id);

		this._type = (type => {
			if (typeof type === 'string') {
				switch (type.toLowerCase()) {
					case "script":
						return ResourceFileType.SCRIPT;
					case "style":
						return ResourceFileType.STYLE;
					default:
						return ResourceFileType.USER_DEFINED;
				}
			}
		})(data.type);
	}
}

/**
 * Represents a resource defined in a resource json file.
 * A resource can contain several ResourceFiles
 */
class Resource {
	/**
	 * The name of the resource
	 */
	private _name:string;
	/**
	 * List of resource dependencies
	 */
	private _dependencies:Array<string>;
	/**
	 * An array of resource files of this resource
	 */
	private _files:Array<ResourceFile>;

	get name():string {
		return this._name;
	}

	set name(value:string) {
		this._name = value;
	}

	get dependencies():Array<string> {
		return this._dependencies;
	}

	set dependencies(value:Array<string>) {
		this._dependencies = value;
	}

	get files():Array<ResourceFile> {
		return this._files;
	}

	set files(value:Array<ResourceFile>) {
		this._files = value;
	}

	/**
	 * The data property must be a parsed json object of a resource json file.
	 * @param data
	 */
	constructor(data:any) {
		// If the dependencies array is defined, then use it. Otherwise return an empty array.
		this._dependencies = (dependencies => {
			if (Array.isArray(dependencies)) {
				return dependencies;
			}
			return [];
		})(data.dependencies);

		this._files = (files => {
			if (Array.isArray(files)) {
				return files.map(file => new ResourceFile(file))
			}
			// By throwing an error we force the users to define at least one valid file for each resource. So we don't have to worry about them
			throw new Error('Invalid resource file. The resource file list (files) property is not an array.');
		})(data.files);

	}
}

/**
 * Load the resource json files from the resources directory. (default.json + <hostname>.json)
 * This will throw an error if the default.json file does not exists
 */
function loadResourceObject() {

	let defaultFile = parseResourceFile('../resources/default.json', null);
	if (defaultFile == null) throw new Error('There was an error loading the default resource file');
	// let systemSpecificFile = parseResourceFile(`../resources/${HOSTNAME}.json`, {});
	let systemSpecificFile = fs.existsSync(process.argv[4]) ? parseResourceFile(process.argv[4], {}) : parseResourceFile("../resources/" + HOSTNAME + ".json", {});

	let combinedFile = deepExtend(defaultFile, systemSpecificFile);

	for (let key in combinedFile) {
		if (combinedFile.hasOwnProperty(key)) {
			resourcesData.set(key, new Resource(combinedFile[key]));
		}
	}
}

function parseResourceFile(filePath:string, defaultValue:any):any {
	let tmp;
	try {
		logger.info(`Loading resource file: ${filePath}`);
		tmp = JSON.parse(fs.readFileSync(filePath, 'utf8'));
	} catch (error) {
		logger.warn(`Unable and unwilling to load resource files from file system: ${filePath}`);
		return defaultValue;
	}
	return tmp;
}

function init() {
	loadResourceObject();
}

{
	init();
}

export function getResourceFilePath(resourceName:string): string {
	let names = resourceName.split('.');

	if (Array.isArray(names) && names.length === 2) {
		try {
			return resourcesData.get(names[0]).files.find(file => file.id === names[1]).src;
		} catch(error) {
			logger.warn(`Could not locate resource file with resource identifier ${resourceName}`);
			return null;
		}
	}
	return null;

}

/**
 * Extening object that entered in first argument.
 *
 * Returns extended object or false if have no target object or incorrect type.
 *
 * If you wish to clone source object (without modify it), just use empty new
 * object as first argument, like this:
 *   deepExtend({}, yourObj_1, [yourObj_N]);
 */
function deepExtend(...args:any[]) {
	// convert arguments to array and cut off target object
	var target = args[0];
	var val, src, clone;

	args.forEach(function (obj) {
		// skip argument if it is array or isn't object
		if (typeof obj !== 'object' || Array.isArray(obj)) {
			return;
		}

		Object.keys(obj).forEach(function (key) {
			src = target[key]; // source value
			val = obj[key]; // new value

			// recursion prevention
			if (val === target) {
				return;

				/**
				 * if new value isn't object then just overwrite by new value
				 * instead of extending.
				 */
			} else if (typeof val !== 'object' || val === null) {
				target[key] = val;
				return;

				// just clone arrays (and recursive clone objects inside)
			} else if (Array.isArray(val)) {
				target[key] = deepCloneArray(val);
				return;

				// custom cloning and overwrite for specific objects
			} else if (isSpecificValue(val)) {
				target[key] = cloneSpecificValue(val);
				return;

				// overwrite by new value if source isn't object or array
			} else if (typeof src !== 'object' || src === null || Array.isArray(src)) {
				target[key] = deepExtend({}, val);
				return;

				// source value and new value is objects both, extending...
			} else {
				target[key] = deepExtend(src, val);
				return;
			}
		});
	});

	return target;
}

/**
 * Recursive cloning array.
 */
function deepCloneArray(arr) {
	var clone = [];
	arr.forEach(function (item, index) {
		if (typeof item === 'object' && item !== null) {
			if (Array.isArray(item)) {
				clone[index] = deepCloneArray(item);
			} else if (isSpecificValue(item)) {
				clone[index] = cloneSpecificValue(item);
			} else {
				clone[index] = deepExtend({}, item);
			}
		} else {
			clone[index] = item;
		}
	});
	return clone;
}

// Functions for extending an object:
function isSpecificValue(val) {
	return !!(
		val instanceof Buffer
		|| val instanceof Date
		|| val instanceof RegExp
	);
}

function cloneSpecificValue(val):any {
	if (val instanceof Buffer) {
		var x = new Buffer(val.length);
		val.copy(x);
		return x;
	} else if (val instanceof Date) {
		return new Date(val.getTime());
	} else if (val instanceof RegExp) {
		return new RegExp(val);
	} else {
		throw new Error('Unexpected situation');
	}
}
