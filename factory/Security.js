"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var SecurityManager = (function () {
    function SecurityManager() {
        this.checkLoggedIn = function (sess) {
            if (sess.logged_in)
                return true;
            return false;
        };
    }
    return SecurityManager;
}());
exports.SecurityManager = SecurityManager;
