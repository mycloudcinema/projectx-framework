/*
 ____________                                           _
 |___  /  ____|                                        | |
   / /| |__ _ __ __ _ _ __ ___   _____      _____  _ __| | __
  / / |  __| '__/ _` | '_ ` _ \ / _ \ \ /\ / / _ \| '__| |/ /
 / /__| |  | | | (_| | | | | | |  __/\ V  V / (_) | |  |   <
/_____|_|  |_|  \__,_|_| |_| |_|\___| \_/\_/ \___/|_|  |_|\_\

Author:		Forisz
Date:		06/04/15.

Handles all security functionality for the Framework application
*/

///<reference path='../tsc-descriptors/express.d.ts'/>
///<reference path='../tsc-descriptors/node/node.d.ts'/>
///<reference path='../tsc-descriptors/mysql.d.ts'/>

export class SecurityManager {
	public checkLoggedIn = (sess) => {
		// Check if the session is not expired and the user is logged in.
		// TODO: Check wether the user session is expired
		if (sess.logged_in) return true;
		return false;
	}
}
