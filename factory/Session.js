"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.securityManager = exports.validateRequest = exports.SessionManager = void 0;
var mysql = require("mysql");
var Utils = require("./Utils");
var moment = require("moment");
var Settings = require("../external/Settings");
var Logger_1 = require("../external/Logger");
var crypto = require("crypto");
var settings = Settings;
var logger = Logger_1.getLogger("Session Manager", settings);
var log = new Utils.Logger("SessionManager", 0).log;
var ONE_YEAR = 365 * 24 * 60 * 60 * 1000;
var TWO_YEARS = 2 * ONE_YEAR;
var REMEMBER_COOKIE_NAME = "zfremember";
var DEFAULT_REMEMBER_COOKIE_LENGTH = 64;
var REMEMBER_COOKIE_LENGTH = settings.session.rememberCookieLength || DEFAULT_REMEMBER_COOKIE_LENGTH;
var SessionManager = (function () {
    function SessionManager() {
        var _this = this;
        this.Login = function (request, response, success, failure) {
            try {
                logger.debug("Create login connection");
                logger.debug("User's IP address is " + request.ip_address);
                var connection = mysql.createConnection(settings.session.loginConnection);
                logger.debug("Login connection created");
                connection.query("call get_user(?)", [request.body.username], function (err, results) {
                    logger.debug("Loading data for user with login: " + request.body.username);
                    connection.end();
                    if (err) {
                        logger.error("There was an error while loading user data", err);
                    }
                    else {
                        if (Object.prototype.toString.call(results) === '[object Array]') {
                            var query_details = results[1], query_response = results[0];
                            if (typeof query_response[0] !== "undefined") {
                                if (Utils.checkPasswordMatch(request.body.password, query_response[0].password)) {
                                    request.session.user = query_response[0];
                                    request.session.user.elevatedRights = {};
                                    request.session.user.session_data = {};
                                    var expires = moment().add(settings.session.expires, "minutes");
                                    request.session.user.session_expires = expires.toISOString();
                                    request.session.ip_address = request.ip;
                                    request.session.remember = _this.handleRememberMe(request, response);
                                    success(query_response[0]);
                                }
                                else
                                    failure();
                            }
                            else
                                failure();
                        }
                        else
                            failure();
                    }
                });
            }
            catch (e) {
                logger.error("Failed to load user data.", e);
            }
        };
        this.grantElevatedRight = function (request, action, success, failure) {
            logger.debug("Create connection to the User's database");
            var connection = mysql.createConnection(settings.session.loginConnection);
            connection.connect();
            connection.query("call get_user(?)", [request.body.username], function (error, results) {
                logger.debug("Loading data for user with login: " + request.body.username);
                connection.end();
                if (Array.isArray(results)) {
                    var query_details = results[1], query_response = results[0];
                    if (typeof query_response !== "undefined") {
                        if (Utils.checkPasswordMatch(request.body.password, query_response[0].password)) {
                            var securityManager = new SecurityManager();
                            if (securityManager.hasAccessLite(query_response[0], action)) {
                                this.addElevatedRight(request.user, action);
                                success();
                                return;
                            }
                        }
                    }
                }
                failure();
            });
        };
        this.isLoggedIn = function (request) {
            logger.debug("Checking if user is logged in", request.session.user);
            if (typeof request.session.user !== "undefined" && request.session.user.login) {
                logger.debug("User session exists. Checking the remember me cookie (" + REMEMBER_COOKIE_NAME + ")");
                if (request.session.remember && request.cookies[REMEMBER_COOKIE_NAME] && request.session.remember === request.cookies[REMEMBER_COOKIE_NAME]) {
                    var expires = moment().add(settings.session.expires, "minutes").toISOString();
                    request.session.user.session_expires = expires;
                    logger.debug("Remember me cookie validated, extending session timeout to " + expires);
                    return true;
                }
                else {
                    logger.debug("Remember me cookie does not exist. Continue regular session validation");
                    if (request.ip === request.session.ip_address) {
                        logger.debug("IP address match. User identified.", request.session.user);
                        var difference = moment().diff(request.session.user.session_expires);
                        if (difference < 0) {
                            var expires = moment().add(settings.session.expires, "minutes");
                            request.session.user.session_expires = expires.toISOString();
                            logger.debug("Session expires at " + request.session.user.session_expires, request.session.user);
                            return true;
                        }
                        else
                            logger.debug("Session expired.", request.session.user);
                    }
                    else {
                        logger.warn("The IP address from the request " + request.ip + " does not match the user's saved IP " + request.session.ip_address + ". Possible sesssion hijacking");
                        logger.info("Redirecting to the login page");
                    }
                }
            }
            return false;
        };
        logger.debug("Remember cookie length set to: " + REMEMBER_COOKIE_LENGTH + " bytes (hexa)");
    }
    SessionManager.prototype.handleRememberMe = function (request, response) {
        if (request.body.remember === 'true') {
            logger.debug("The user selected the remember me option. " + JSON.stringify(request.body.remember));
            var bytes = crypto.randomBytes(REMEMBER_COOKIE_LENGTH).toString('hex');
            response.cookie(REMEMBER_COOKIE_NAME, bytes, { maxAge: TWO_YEARS });
            return bytes;
        }
        return false;
    };
    SessionManager.prototype.setUserSessionData = function (request, data) {
        try {
            request.session.user.session_data = Object.assign(request.session.user.session_data, data);
        }
        catch (ex) {
            logger.error(ex);
        }
    };
    SessionManager.prototype.getUserSessionData = function (request, key) {
        try {
            if (key)
                return request.session.user.session_data[key];
            return request.session.user.session_data;
        }
        catch (ex) {
            return null;
        }
    };
    SessionManager.prototype.addElevatedRight = function (user, action) {
        if (user.elevatedRights.hasOwnProperty(action)) {
            user.elevatedRights[action].available = 1;
        }
        else {
            user.evevatedRights[action] = {
                available: 1
            };
        }
    };
    return SessionManager;
}());
exports.SessionManager = SessionManager;
var SecurityPool = (function () {
    function SecurityPool() {
        var _this = this;
        this.getFunctions = function (user_role_id) {
            if (_this.pool.hasOwnProperty(user_role_id)) {
                return _this.pool[user_role_id];
            }
            logger.debug("User role " + user_role_id + " does not exist in the security pool");
            return [];
        };
        this.setPool = function (data) {
            for (var i = 0; i < data.length; i++) {
                var item = data[i];
                if (_this.pool.hasOwnProperty(item.user_role_id) && Array.isArray(_this.pool[item.user_role_id])) {
                    _this.pool[item.user_role_id].push(item.function_name);
                }
                else {
                    _this.pool[item.user_role_id] = [item.function_name];
                }
            }
        };
        this.pool = {};
    }
    return SecurityPool;
}());
var SecurityManager = (function () {
    function SecurityManager() {
        this.logger = Logger_1.getLogger("Security Manager", settings);
        this.securityPool = new SecurityPool();
        this.loadFunctions();
    }
    SecurityManager.prototype.hasAccess = function (user, action) {
        if (settings.app.public_website) {
            this.logger.info("Access granted for action: " + action + " on a public website", user);
            return true;
        }
        if (settings.session.nonSecureItems.indexOf(action) !== -1) {
            this.logger.info("Access granted for action: " + action, user);
            return true;
        }
        this.logger.debug("Checking whether the user has access to " + action, user);
        if (settings.session.nonSecureItems.indexOf(action) !== -1) {
            this.logger.info("Access granted for action: " + action, user);
            return true;
        }
        var roles = JSON.parse(user.user_roles);
        for (var i = 0; i < roles.length; i++) {
            var functions = this.securityPool.getFunctions(roles[i]);
            if (functions.indexOf(action) !== -1) {
                this.logger.info("Access granted for action: " + action, user);
                return true;
            }
        }
        this.logger.warn("Access denied for action " + action, user);
        return false;
    };
    SecurityManager.prototype.hasAccessLite = function (user, action) {
        this.logger.debug("Checking whether the user has access to " + action, user);
        if (settings.app.public_website) {
            this.logger.info("Access granted for action: " + action + " on a public website", user);
            return true;
        }
        if (settings.session.nonSecureItems.indexOf(action) !== -1) {
            this.logger.info("Access granted for action: " + action, user);
            return true;
        }
        var roles = JSON.parse(user.user_roles);
        for (var i = 0; i < roles.length; i++) {
            var functions = this.securityPool.getFunctions(roles[i]);
            if (functions.indexOf(action) !== -1) {
                this.logger.info("Access granted for action: " + action, user);
                return true;
            }
        }
        this.logger.warn("Access denied for action " + action, user);
        return false;
    };
    SecurityManager.prototype.reload = function () {
        this.securityPool = new SecurityPool();
        this.loadFunctions();
    };
    ;
    SecurityManager.prototype.loadFunctions = function () {
        var _this = this;
        var connection = this.createSecurityConnection();
        this.logger.info("Loading security information");
        connection.query({
            sql: "CALL get_security_userrolefunctions(?)",
            timeout: (settings.app.db_timeout || 30000),
        }, [settings.app.site_name], function (error, rows, fields) {
            _this.logger.debug("Security information loaded from the DB");
            if (error) {
                _this.logger.error("There was an error while loading security information from the database", error);
            }
            else {
                _this.securityPool.setPool(rows[0]);
            }
            connection.end();
        });
    };
    SecurityManager.prototype.createSecurityConnection = function () {
        try {
            if (!settings.security.connection.connectTimeout) {
                settings.security.connection.connectTimeout = settings.app.db_timeout || 10000;
            }
            if (!settings.security.connection.timeout) {
                settings.security.connection.timeout = settings.app.db_timeout || 30000;
            }
        }
        catch (ex) {
            logger.error(ex);
        }
        var connection = mysql.createConnection(settings.security.connection);
        connection.connect();
        logger.debug("Security connection established " + connection.config.host);
        return connection;
    };
    return SecurityManager;
}());
function validateRequest(req, res) {
    logger.debug("validating request", { url: req.url, settings: settings.session.redirectIfMissing });
    if (Array.isArray(settings.session.redirectIfMissing) && settings.session.redirectIfMissing.length > 0) {
        if (settings.session.nonSecureItems.indexOf(req.url) === -1) {
            for (var i = 0; i < settings.session.redirectIfMissing.length; i++) {
                var item = settings.session.redirectIfMissing[i];
                if (!item.pages.some(function (page) { return isPageMatch(page, req.url); })) {
                    continue;
                }
                if (Array.isArray(item.keys) && item.keys.length > 0) {
                    for (var j = 0; j < item.keys.length; j++) {
                        var key = item.keys[j];
                        try {
                            if (!req.session.user.session_data.hasOwnProperty(key)) {
                                logger.warn("Session variable " + key + " is missing. Redirecting to: " + item.to);
                                req.url = item.to;
                                return;
                            }
                            if (req.session.user.session_data[key] === null) {
                                logger.warn("Session variable " + key + " is missing. Redirecting to: " + item.to);
                                req.url = item.to;
                                return;
                            }
                        }
                        catch (ex) {
                            logger.error(ex);
                        }
                    }
                }
            }
        }
    }
}
exports.validateRequest = validateRequest;
function isPageMatch(page, url) {
    return !!url.match(new RegExp(page));
}
var s = new SecurityManager();
exports.securityManager = new SecurityManager();
