"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TemplateManager = exports.TemplateCache = void 0;
var fs = require("fs-extra");
var Utils = require("./Utils");
var cheerio = require("cheerio");
var Settings = require("../external/Settings");
var UglifyJS = require("uglify-js");
var UglifyCSS = require("uglifycss");
var uuid = require("node-uuid");
var Dictionary_1 = require("./Dictionary");
var Session_1 = require("./Session");
var Resources = require("./Resources");
var Logger_1 = require("../external/Logger");
var settings = Settings;
var log = new Utils.Logger("TemplateManager").log;
var logger = Logger_1.getLogger("Template Manager", settings);
var dictionaryManager = new Dictionary_1.DictionaryManager();
var processLocation = process.cwd();
var test;
var HOSTNAME = require('os').hostname();
var TemplateCache = (function () {
    function TemplateCache() {
        var _this = this;
        this.put = function (template) {
            try {
                _this.template_pool[template.lang][template.template_name] = template;
            }
            catch (e) {
                log("Template Cache - Set: " + e);
            }
            var t = setTimeout(function () {
                try {
                    delete _this.template_pool[template.lang.toUpperCase()][template.template_name];
                }
                catch (e) {
                    log("Template Cache - Timeout Exception: " + e);
                }
            }, template.cache_interval);
        };
        this.get = function (template_name, lang) {
            return _this.template_pool[lang.toUpperCase()][template_name];
        };
        this.isCached = function (template_name, lang) {
            return (typeof _this.template_pool[lang] !== "undefined" && _this.template_pool[lang].hasOwnProperty(template_name));
        };
        this.template_pool = {};
        for (var i = 0; i < settings.template.supportedLanguages.length; i++) {
            this.template_pool[settings.template.supportedLanguages[i].toUpperCase()] = {};
        }
    }
    return TemplateCache;
}());
exports.TemplateCache = TemplateCache;
var TemplateManager = (function () {
    function TemplateManager(asset_path) {
        var _this = this;
        this.parseResourceTags = function ($, lang) {
            var alreadyAddedResources = [];
            $("resource").each(function (i, element) {
                var $this = $(element);
                var name = $this.attr("name");
                var tags = "";
                if (name !== null) {
                    logger.debug("Checking resources list for " + name);
                    if (_this.resources.hasOwnProperty(name)) {
                        var res = _this.resources[name], tag;
                        for (var j = 0; j < res.length; j++) {
                            if (res[j].type === "script" && alreadyAddedResources.indexOf(res[j].path) === -1) {
                                alreadyAddedResources.push(res[j].path);
                                tags += "<script src='" + res[j].path + "'" + (res[j].avoid_merge || res[j].module ? ' avoid-merge' : '') + (res[j].crossorigin ? ' crossorigin' : '') + (res[j].module ? " type='module'" : '') + (res[j].babel ? " type='text/babel'" : '') + " ></script>";
                            }
                            else if (res[j].type === "link" && alreadyAddedResources.indexOf(res[j].path) === -1) {
                                alreadyAddedResources.push(res[j].path);
                                tags += "<link rel='stylesheet' href='" + res[j].path + "'" + (res[j].avoid_merge ? ' avoid-merge' : '') + (res[j].crossorigin ? ' crossorigin' : '') + " />";
                            }
                        }
                    }
                    else {
                        logger.warn("Could not find resource: " + name);
                    }
                }
                $this.replaceWith($(tags));
            });
            var localeFiles;
            if (_this.resources.hasOwnProperty(lang)) {
                logger.debug("Locale files found for locale (" + lang.toUpperCase() + ")");
                localeFiles = _this.resources[lang.toUpperCase()];
            }
            else {
                logger.debug("Could not find locale files for the current locale " + lang + ". Loading defaults");
                if (_this.resources.hasOwnProperty(settings.template.defaultLanguage)) {
                    localeFiles = _this.resources[settings.template.defaultLanguage];
                }
                else {
                    logger.warn("Could not find default locale files. You must have a locale entry in the locales.json file for the default language");
                }
            }
            var tags2 = "";
            if (localeFiles) {
                for (var i = 0; i < localeFiles.length; i++) {
                    var localeFile = localeFiles[i];
                    if (localeFile.type === "script") {
                        tags2 += "<script src='" + localeFile.path + "' " + (localeFile.avoid_merge ? 'avoid-merge' : '') + " " + (localeFile.crossorigin ? 'crossorigin' : '') + " ></script>";
                    }
                    else if (localeFile.type === "link") {
                        tags2 += "<link rel='stylesheet' href='" + localeFile.path + "' " + (localeFile.avoid_merge ? 'avoid-merge' : '') + " " + (localeFile.crossorigin ? 'crossorigin' : '') + " />";
                    }
                }
                $("head").append($(tags2));
            }
        };
        this.parseAccessControlTags = function (source, session, level) {
            var thisSource = source;
            try {
                if (thisSource.search(/<exclude\s*.*>/) !== -1 || thisSource.search(/<include\s*.*>/) !== -1) {
                    var $ = cheerio.load(thisSource, { normalizeWhitespace: settings.cache.minifyHtml || true });
                    var user_roles = JSON.parse(session.user.user_role_names);
                    _this.parseExcludeTags($, user_roles);
                    _this.parseIncludeTags($, user_roles);
                    thisSource = $.html();
                }
                if (thisSource.search(/<access\s*.*>/) !== -1) {
                    var $ = cheerio.load(thisSource, { normalizeWhitespace: settings.cache.minifyHtml || true });
                    _this.parseAccessTags($, session.user);
                    thisSource = $.html();
                }
                if (thisSource.search(/<noaccess\s*.*>/) !== -1) {
                    var $ = cheerio.load(thisSource, { normalizeWhitespace: settings.cache.minifyHtml || true });
                    _this.parseNoAccessTags($, session.user);
                    thisSource = $.html();
                }
                if ((thisSource.search(/<exclude\s*.*>/) !== -1 || thisSource.search(/<include\s*.*>/) !== -1) ||
                    (thisSource.search(/<access\s*.*>/) !== -1) ||
                    (thisSource.search(/<noaccess\s*.*>/) !== -1)) {
                    logger.warn("Processing embedded access control tags at level " + level);
                    if (level < 3) {
                        level++;
                        return _this.parseAccessControlTags(thisSource, session, level);
                    }
                    else {
                        logger.warn("Document contains unreachable access control tags (commented out?).");
                        return thisSource;
                    }
                }
                else {
                    return thisSource;
                }
            }
            catch (exception) {
                logger.error("There was an error parsing access control tags.", exception);
            }
        };
        this.parseExcludeTags = function ($, user_roles) {
            $("exclude").each(function (i, element) {
                var $this = $(this);
                var part_user_roles = $this.attr("user-roles").replace(/\s/g, '').split(",");
                for (var i = 0; i < part_user_roles.length; i++) {
                    if (user_roles.indexOf(part_user_roles[i]) !== -1) {
                        $this.replaceWith('');
                        return;
                    }
                }
                $this.replaceWith($this.html());
            });
        };
        this.parseIncludeTags = function ($, user_roles) {
            $("include").each(function (i, element) {
                var $this = $(this);
                var part_user_roles = $this.attr("user-roles").replace(/\s/g, '').split(",");
                for (var i = 0; i < part_user_roles.length; i++) {
                    if (user_roles.indexOf(part_user_roles[i]) !== -1) {
                        $this.replaceWith($this.html());
                        return;
                    }
                }
                $this.replaceWith('');
            });
        };
        this.parseAccessTags = function ($, user) {
            $("access").each(function (i, element) {
                var $this = $(this);
                var part_functions = $this.attr("functions").replace(/\s/g, '').split(",");
                part_functions.forEach(function (function_name) {
                    if (Session_1.securityManager.hasAccess(user, function_name)) {
                        $this.replaceWith($this.html());
                        return;
                    }
                });
                $this.replaceWith('');
            });
        };
        this.parseNoAccessTags = function ($, user) {
            $("noaccess").each(function (i, element) {
                var $this = $(this);
                var part_functions = $this.attr("functions").replace(/\s/g, '').split(",");
                part_functions.forEach(function (function_name) {
                    if (!Session_1.securityManager.hasAccess(user, function_name)) {
                        $this.replaceWith($this.html());
                        return;
                    }
                });
                $this.replaceWith('');
            });
        };
        this.parseScriptContent = function (input, template_name, language, session) {
            try {
                logger.debug("Processing script part tags");
                input = input.replace(/\$part{\'([a-z,@,\-,.,_,\/,\d]+)\'}/g, function (match, key) {
                    var keyParts = key.split(",");
                    if (keyParts.length > 1) {
                        logger.debug("Processing script part tag " + keyParts[0] + " with access permissions " + keyParts[1]);
                        if (!Session_1.securityManager.hasAccess(session.user, keyParts[1])) {
                            logger.error("Script Part " + keyParts[0] + " was not injected as the user has no access to " + keyParts[1]);
                            return '';
                        }
                    }
                    else {
                        logger.debug("Processing script part tag " + keyParts[0]);
                    }
                    keyParts[0].replace(/^\/+/g, '');
                    try {
                        return _this.parseScriptContent(fs.readFileSync(processLocation + "/" + keyParts[0] + ".js", "UTF-8"), template_name, language, session) + "\n";
                    }
                    catch (ex) {
                        logger.error("Script Part " + keyParts[0] + " was not injected as the file was not found");
                        logger.error(ex);
                        return "Script Part " + keyParts[0] + " was not injected as the file was not found";
                    }
                });
                input = input.replace(/\$user{([a-z,_,\d]+)}/g, function (match, key) {
                    try {
                        var item = session.user[key];
                        if (item) {
                            return item;
                        }
                        else {
                            return "";
                        }
                    }
                    catch (ex) {
                        return "%user: " + key + "%";
                    }
                });
                input = input.replace(/\$user{([a-z,_,\d]+)}/g, function (match, key) {
                    try {
                        var item = session.user[key];
                        if (item) {
                            return item;
                        }
                        else {
                            return "";
                        }
                    }
                    catch (ex) {
                        return "%user: " + key + "%";
                    }
                });
                input = _this.parseSessionTags(input, session);
                input = _this.parseSecurityTags(input, session);
                input = input.replace(/\$language{}/g, function (match, key) {
                    return session.language.toLowerCase() || settings.template.defaultLanguage;
                });
                input = input.replace(/\$setting{([a-z,_,\d]+)}/g, function (match, key) {
                    switch (key) {
                        case 'faye_server':
                            return settings.faye.server;
                        case 'linked_sites':
                            return settings.linked_sites;
                        default:
                            if (settings.user.hasOwnProperty(key)) {
                                return settings.user[key];
                            }
                            else {
                                return "%setting: " + key + "%";
                            }
                    }
                });
                input = input.replace(/\$dict{([a-z,_,\d]+)}/g, function (match, key) {
                    var dictItem = dictionaryManager.getDictionaryItem(language, key);
                    if (dictItem !== null)
                        return dictItem;
                    return "%" + key + "%";
                });
                input = input.replace(/\$resource\{([a-z,_,\.,\d]+)\}/g, function (match, key) {
                    var filePath = Resources.getResourceFilePath(key);
                    if (filePath) {
                        return filePath;
                    }
                    return "%" + key + "%";
                });
                input = input.replace(/\$templatename\{\}/g, function (match) { return template_name; });
            }
            catch (ex) {
                logger.error(ex);
            }
            return input;
        };
        this.getCacheInterval = function ($) {
            var timeout = (settings.cache.defaultCacheInterval.seconds * 1000) +
                (settings.cache.defaultCacheInterval.minutes * 60000) +
                (settings.cache.defaultCacheInterval.hours * 3600000) +
                (settings.cache.defaultCacheInterval.days * 86400000) +
                (settings.cache.defaultCacheInterval.weeks * 604800000);
            var templateLevelCache = $("cache").attr("timeout");
            $("cache").each(function (i, element) {
                $(element).replaceWith("");
            });
            if (typeof templateLevelCache !== "undefined") {
                var weeks = templateLevelCache.match(/(\d+)w/) ? parseInt(templateLevelCache.match(/(\d+)w/)[1]) : 0;
                var days = templateLevelCache.match(/(\d+)d/) ? parseInt(templateLevelCache.match(/(\d+)d/)[1]) : 0;
                var hours = templateLevelCache.match(/(\d+)h/) ? parseInt(templateLevelCache.match(/(\d+)h/)[1]) : 0;
                var minutes = templateLevelCache.match(/(\d+)m/) ? parseInt(templateLevelCache.match(/(\d+)m/)[1]) : 0;
                var seconds = templateLevelCache.match(/(\d+)s/) ? parseInt(templateLevelCache.match(/(\d+)s/)[1]) : 0;
                timeout = (seconds * 1000) +
                    (minutes * 60000) +
                    (hours * 3600000) +
                    (days * 86400000) +
                    (weeks * 604800000);
                logger.info("Caching template for " + weeks + " weeks " + days + " days " + hours + " hours " + minutes + " minutes " + seconds + " seconds");
            }
            return timeout;
        };
        this.getTemplate = function (template_name, lang, session) {
            if (_this.templateCache.isCached(template_name, lang)) {
                logger.debug("Loading template " + template_name + " from cache");
                var template = _this.templateCache.get(template_name, lang);
                template.source = _this.parseSessionTags(template.source, session);
                template.source = _this.parseAccessControlTags(template.source, session);
                return template;
            }
            else {
                var $ = _this.$;
                logger.debug("Loading template " + template_name + " from disk");
                if (fs.existsSync(_this.asset_path + template_name + ".html")) {
                    var mainPart = fs.readFileSync(_this.asset_path + template_name + ".html", "UTF-8");
                    var matches = mainPart.match(/<base-template[\s*]name="(.+)">/);
                    mainPart = mainPart.replace(/<base-template[\s*]name="(.*)"><\/base-template>/, '');
                    var baseTemplate = void 0;
                    if (matches && matches.length > 1 && fs.existsSync(_this.asset_path + matches[1] + ".html")) {
                        baseTemplate = fs.readFileSync(_this.asset_path + matches[1] + ".html", "UTF-8");
                    }
                    else {
                        baseTemplate = _this.base_template;
                        logger.info("Base template not defined or missing for (" + template_name + "). The default base template (" + settings.template.defaultBaseTemplateName + ") will be used.");
                    }
                    $ = cheerio.load(baseTemplate, { normalizeWhitespace: settings.cache.minifyHtml || true });
                    $("main-part").replaceWith(mainPart);
                    _this.parsePartTags($);
                    _this.parseDictionaryTags($, lang);
                    _this.parseResourceTags($, lang);
                    _this.parseResources($, template_name, lang, session);
                    var templateSource = $.html();
                    templateSource = templateSource.replace(/\$dict{([a-z,_,\d]+)}/g, function (match, key) {
                        var dictItem = dictionaryManager.getDictionaryItem(lang, key);
                        if (dictItem !== null)
                            return dictItem;
                        return "%" + key + "%";
                    });
                    templateSource = templateSource.replace(/\$language{}/g, function (match, key) {
                        return session.language.toLowerCase() || settings.template.defaultLanguage;
                    });
                    templateSource = templateSource.replace(/\$resource\{([a-z,_,\.,\d]+)\}/g, function (match, key) {
                        var filePath = Resources.getResourceFilePath(key);
                        if (filePath) {
                            return filePath;
                        }
                        return "%" + key + "%";
                    });
                    templateSource = templateSource.replace(/\$templatename\{\}/g, function (match) { return template_name; });
                    templateSource = _this.parseAccessControlTags(templateSource, session);
                    var tpl = {
                        lang: lang,
                        source: templateSource,
                        template_name: template_name,
                        cache_interval: _this.getCacheInterval($)
                    };
                    _this.templateCache.put(Utils.copy(tpl));
                    tpl.source = _this.parseSessionTags(tpl.source, session);
                    tpl.source = tpl.source.replace(/<cache[\s*]timeout="(.*)"><\/cache>/, '');
                    return tpl;
                }
                else
                    return null;
            }
        };
        this.asset_path = processLocation + "/" + settings.template.assetPath + "/pagetemplates/";
        test = asset_path;
        this.base_template = fs.readFileSync(this.asset_path + settings.template.defaultBaseTemplateName + ".html", "UTF-8");
        this.templateCache = new TemplateCache(60);
        var res = this.loadResources();
        if (settings.template.compressResources) {
            this.resources = this.compressResources(res);
        }
        else {
            this.resources = res;
        }
        dictionaryManager.loadDictionary();
    }
    TemplateManager.prototype.parsePartTags = function ($) {
        var $parts = $("part");
        while ($parts.length !== 0) {
            $parts.each(function (i, elemenet) {
                var $this = $(this);
                $this.replaceWith(fs.readFileSync(processLocation + "/" + settings.template.assetPath + "/pagetemplates/" + $this.attr("name") + ".html", "UTF-8") + "\n");
            });
            $parts = $("part");
        }
    };
    TemplateManager.prototype.parseDictionaryTags = function ($, language) {
        $("dict").each(function (i, element) {
            var $this = $(this);
            var dictItem = dictionaryManager.getDictionaryItem(language, $this.attr("key"));
            if (dictItem) {
                $this.replaceWith(dictItem);
            }
            else {
                $this.replaceWith("%" + $this.attr("key") + "%");
            }
        });
    };
    TemplateManager.prototype.parseSessionTags = function (source, session) {
        try {
            return source.replace(/\$sess{([a-z,_,\d]+)}/g, function (match, key) {
                try {
                    return session.user[key] || "$sess%" + key + "%";
                }
                catch (ex) {
                    return "";
                }
            }).replace(/\$session{([a-z,_,\d]+)}/g, function (match, key) {
                try {
                    return session.user.session_data[key] || "$session%" + key + "%";
                }
                catch (ex) {
                    return "";
                }
            });
        }
        catch (ex) {
            logger.error(ex);
            return source;
        }
    };
    TemplateManager.prototype.parseSecurityTags = function (source, session) {
        logger.info('Parsing security tags');
        try {
            return source.replace(/\$access{([a-z,_,\d,\/]+)}/g, function (match, function_name) {
                try {
                    if (Session_1.securityManager.hasAccess(session.user, function_name)) {
                        return 'true';
                    }
                    else {
                        return 'false';
                    }
                }
                catch (ex) {
                    return 'false';
                }
            });
        }
        catch (ex) {
            logger.error(ex);
            return source;
        }
    };
    ;
    TemplateManager.prototype.parseApplicationTags = function () {
    };
    TemplateManager.prototype.parseCustomTags = function () {
    };
    TemplateManager.prototype.parseResources = function ($, template_name, language, session) {
        var _this = this;
        logger.debug("Parsing resources for " + template_name);
        var $head = $("head");
        var scripts = "";
        var stylesheets = "";
        var name = language.toLowerCase() + "_" + template_name.replace(/\//g, '');
        $("script").each(function (i, element) {
            var $this = $(this);
            logger.trace("Processing resource " + $this);
            if ($this.attr("src").startsWith('http')) {
                logger.debug("External resource " + $this.attr("src") + " is included but not injected in the compressed JavaScript");
            }
            else if ($this.attr("type") === "module") {
                logger.debug("Injecting " + $this.attr("src") + " as a module");
            }
            else if ($this.attr("type") === "text/babel") {
                logger.debug("Injecting " + $this.attr("src") + " as babel");
            }
            else if (typeof $this.attr("avoid-merge") === "undefined" && typeof $this.attr("src") !== "undefined") {
                try {
                    scripts += fs.readFileSync(processLocation + "/../" + $this.attr("src").replace("/", ""), "UTF-8") + "\n";
                    $this.remove();
                }
                catch (exception) {
                    logger.error("Could not read resource file: " + $this.attr("src").replace("/", "") + ".", exception);
                }
            }
        });
        var scriptPath = processLocation + "/" + settings.template.assetPath + "/scripttemplates" + template_name + ".js";
        try {
            var mainScriptTemplate = fs.readFileSync(scriptPath, "UTF-8") + "\n";
            scripts += mainScriptTemplate;
            scripts = (function (input) {
                return _this.parseScriptContent(input, template_name, language, session);
            })(scripts);
        }
        catch (error) {
            logger.error("Could not locate script file for template " + template_name + ". Check the scripttemplates " + template_name + ".js file.", error);
        }
        $("link").each(function (i, element) {
            var $this = $(this);
            if (typeof $this.attr("avoid-merge") === "undefined" && typeof $this.attr("href") !== "undefined") {
                try {
                    stylesheets += fs.readFileSync(processLocation + "/../" + $(this).attr("href"), "UTF-8");
                }
                catch (exception) {
                    logger.error("Could not read resource file: " + $this.attr("href") + ".", exception);
                }
                $this.remove();
            }
        });
        if (fs.existsSync(processLocation + "/../public/generated/js" + name + ".js")) {
            fs.removeSync(processLocation + "/../public/generated/js" + name + ".js");
            fs.writeFileSync(processLocation + "/../public/generated/js" + name + ".js", scripts);
        }
        else {
            fs.writeFileSync(processLocation + "/../public/generated/js" + name + ".js", scripts);
        }
        if (fs.existsSync(processLocation + "/../public/generated/css" + name + ".css")) {
            fs.removeSync(processLocation + "/../public/generated/css" + name + ".css");
            fs.writeFileSync(processLocation + "/../public/generated/css" + name + ".css", stylesheets);
        }
        else {
            fs.writeFileSync(processLocation + "/../public/generated/css" + name + ".css", stylesheets);
        }
        $head
            .append("<link rel='stylesheet' href='/public/generated/css" + name + ".css'>")
            .append("<script src='/public/generated/js" + name + ".js'></script>");
    };
    TemplateManager.prototype.loadResources = function () {
        logger.debug("Loading resources for generating resource tags");
        var resources = {};
        var locales = {};
        try {
            resources = JSON.parse(fs.readFileSync(processLocation + "/../data/resources.json", "UTF-8"));
        }
        catch (ex) {
            logger.error("Failed to load resources.json. Please make sure that the file exist and its valid JSON", ex);
        }
        try {
            var defaultFile = parseResourceFile('../locales/default.json', null, true);
            if (defaultFile == null) {
                throw new Error('There was an error loading the default resource file');
            }
            var systemSpecificFile = fs.existsSync(process.argv[3]) ? parseResourceFile(process.argv[3], {}, false) : parseResourceFile("../locales/" + HOSTNAME + ".json", {}, false);
            logger.info('Deep extended default locales with system specific locales');
            var combinedFile = deepExtend(defaultFile, systemSpecificFile);
            for (var key in combinedFile) {
                logger.info("Resources loaded for locale (" + key.toUpperCase() + ")");
                resources[key.toUpperCase()] = combinedFile[key.toUpperCase()];
            }
        }
        catch (ex) {
            logger.error("Failed to load locales.json", ex);
        }
        return resources;
    };
    TemplateManager.prototype.compressResources = function (resources) {
        try {
            logger.info("Compressing resource files...");
            var outputFolder = processLocation + "/../compressed/";
            fs.emptyDirSync(outputFolder);
            logger.debug("Output folder for compressed resources cleared");
            var totalCompressed = 0;
            for (var key in resources) {
                var resource = resources[key];
                for (var i = 0; i < resource.length; i++) {
                    if (resource[i].type === "script" && (!resource[i].hasOwnProperty("uglify") || resource[i].uglify === true)) {
                        var filename = uuid.v4();
                        var relativePath = "/compressed/" + filename;
                        var minifiedFilePath = outputFolder + filename;
                        logger.debug("Compressing: " + resource[i].path);
                        fs.writeFileSync(minifiedFilePath, UglifyJS.minify(processLocation + "/.." + resource[i].path, {
                            compress: {
                                sequences: true,
                                properties: false,
                                dead_code: true,
                                drop_debugger: true,
                                unsafe: false,
                                conditionals: true,
                                comparisons: true,
                                evaluate: true,
                                booleans: true,
                                loops: true,
                                unused: true,
                                hoist_funs: true,
                                hoist_vars: false,
                                if_return: true,
                                join_vars: true,
                                cascade: true,
                                side_effects: true,
                                warnings: false
                            }
                        }, true).code);
                        totalCompressed++;
                        resource[i].path = relativePath;
                    }
                    else if (resource[i].type === "link" && (!resource[i].hasOwnProperty("uglify") || resource[i].uglify === true)) {
                        var filename = uuid.v4();
                        var relativePath = "/compressed/" + filename;
                        var minifiedFilePath = outputFolder + filename;
                        logger.debug("Compressing: " + resource[i].path);
                        fs.writeFileSync(minifiedFilePath, UglifyCSS.processFiles([processLocation + "/.." + resource[i].path], {}));
                        totalCompressed++;
                        resource[i].path = relativePath;
                    }
                }
            }
            logger.info(totalCompressed + " files were succesfully compressed");
            return resources;
        }
        catch (e) {
            logger.error("Failed to compress resources.", e);
        }
    };
    return TemplateManager;
}());
exports.TemplateManager = TemplateManager;
function deepExtend() {
    var args = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        args[_i - 0] = arguments[_i];
    }
    var target = args[0];
    var val, src, clone;
    args.forEach(function (obj) {
        if (typeof obj !== 'object' || Array.isArray(obj)) {
            return;
        }
        Object.keys(obj).forEach(function (key) {
            src = target[key];
            val = obj[key];
            if (val === target) {
                return;
            }
            else if (typeof val !== 'object' || val === null) {
                target[key] = val;
                return;
            }
            else if (Array.isArray(val)) {
                target[key] = deepCloneArray(val);
                return;
            }
            else if (isSpecificValue(val)) {
                target[key] = cloneSpecificValue(val);
                return;
            }
            else if (typeof src !== 'object' || src === null || Array.isArray(src)) {
                target[key] = deepExtend({}, val);
                return;
            }
            else {
                target[key] = deepExtend(src, val);
                return;
            }
        });
    });
    return target;
}
function deepCloneArray(arr) {
    var clone = [];
    arr.forEach(function (item, index) {
        if (typeof item === 'object' && item !== null) {
            if (Array.isArray(item)) {
                clone[index] = deepCloneArray(item);
            }
            else if (isSpecificValue(item)) {
                clone[index] = cloneSpecificValue(item);
            }
            else {
                clone[index] = deepExtend({}, item);
            }
        }
        else {
            clone[index] = item;
        }
    });
    return clone;
}
function isSpecificValue(val) {
    return !!(val instanceof Buffer
        || val instanceof Date
        || val instanceof RegExp);
}
function cloneSpecificValue(val) {
    if (val instanceof Buffer) {
        var x = new Buffer(val.length);
        val.copy(x);
        return x;
    }
    else if (val instanceof Date) {
        return new Date(val.getTime());
    }
    else if (val instanceof RegExp) {
        return new RegExp(val);
    }
    else {
        throw new Error('Unexpected situation');
    }
}
function parseResourceFile(filePath, defaultValue, logError) {
    var tmp;
    try {
        logger.info('Loading resource file: ' + filePath);
        tmp = JSON.parse(fs.readFileSync(filePath, 'utf8'));
    }
    catch (error) {
        if (logError) {
            logger.error('Error while loading locale files from file system: ' + filePath);
            logger.debug(error);
        }
        return defaultValue;
    }
    return tmp;
}
