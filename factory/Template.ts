/*
 ____________                                           _
 |___  /  ____|                                        | |
   / /| |__ _ __ __ _ _ __ ___   _____      _____  _ __| | __
  / / |  __| '__/ _` | '_ ` _ \ / _ \ \ /\ / / _ \| '__| |/ /
 / /__| |  | | | (_| | | | | | |  __/\ V  V / (_) | |  |   <
/_____|_|  |_|  \__,_|_| |_| |_|\___| \_/\_/ \___/|_|  |_|\_\

Author:		Forisz
Date:		06/04/15.

Handles all template requests for the Framework application
*/

///<reference path='../tsc-descriptors/express.d.ts'/>
///<reference path='../tsc-descriptors/cheerio.d.ts'/>
///<reference path='../tsc-descriptors/node/node.d.ts'/>
///<reference path='../tsc-descriptors/uglify-js.d.ts'/>
///<reference path='../tsc-descriptors/uglifycss.d.ts'/>
///<reference path='../tsc-descriptors/node-uuid.d.ts'/>
///<reference path='../tsc-descriptors/fs-extra.d.ts'/>

import express = require('express');
import URL = require('url');
import fs = require("fs-extra");
import Utils = require("./Utils");
import cheerio = require("cheerio");
import Settings = require("../external/Settings");
import color = require("colors");
import UglifyJS = require("uglify-js");
import UglifyCSS = require("uglifycss");
import uuid = require("node-uuid");
import {DictionaryManager} from "./Dictionary";
import fwSession = require("./Session");
import {securityManager} from "./Session";
import * as Resources from './Resources';

import {getLogger} from "../external/Logger";

var settings:Settings.ISettings = Settings;

var log = new Utils.Logger("TemplateManager").log;
const logger = getLogger("Template Manager", settings);

var dictionaryManager = new DictionaryManager();
var processLocation = process.cwd();
var test;
var HOSTNAME = require('os').hostname();

interface ITemplate {
	lang:string;
	template_name:string;
	source:string;
	cache_interval:number; // The amount of milliseconds the template will be cached
}

export class TemplateCache {
	template_pool:Object;
	put = (template:ITemplate):void => {
		try {
			this.template_pool[template.lang][template.template_name] = template;
		} catch (e) {
			log("Template Cache - Set: " + e);
		}
		var t = setTimeout(() => {
			try {
				delete this.template_pool[template.lang.toUpperCase()][template.template_name];
			} catch (e) {
				log("Template Cache - Timeout Exception: " + e);
			}
		}, template.cache_interval);
	};
	get = (template_name:string, lang:string):ITemplate => {
		return this.template_pool[lang.toUpperCase()][template_name];
	};
	isCached = (template_name:string, lang:string):boolean => {
		return (typeof this.template_pool[lang] !== "undefined" && this.template_pool[lang].hasOwnProperty(template_name));
	};

	constructor() {
		this.template_pool = {};
		for (var i = 0; i < settings.template.supportedLanguages.length; i++) {
			this.template_pool[settings.template.supportedLanguages[i].toUpperCase()] = {};
		}
	}
}

export class TemplateManager {

	private asset_path:string;
	private base_template:string;
	private $:CheerioStatic;
	private templateCache:TemplateCache;
	private resources:Object;

	private parsePartTags($:CheerioStatic):void {
		var $parts = $("part");
		while ($parts.length !== 0) {
			$parts.each(function (i, elemenet) {
				var $this = $(this);
				$this.replaceWith(fs.readFileSync(processLocation + "/" + settings.template.assetPath + "/pagetemplates/" + $this.attr("name") + ".html", "UTF-8") + "\n");
			});
			$parts = $("part");
		}
	}

	private parseDictionaryTags($:CheerioStatic, language:string):void {
		$("dict").each(function (i, element) {
			var $this = $(this);
			var dictItem = dictionaryManager.getDictionaryItem(language, $this.attr("key"));
			if (dictItem) {
				$this.replaceWith(dictItem);
			} else {
				$this.replaceWith("%" + $this.attr("key") + "%");
			}

		});
	}

	private parseResourceTags = ($:CheerioStatic, lang:string):void => {

		var alreadyAddedResources = [];

		$("resource").each((i, element) => {

			var $this = $(element);
			var name = $this.attr("name");
			var tags = "";

			if (name !== null) {

				logger.debug(`Checking resources list for ${name}`);
				if (this.resources.hasOwnProperty(name)) {

					var res = this.resources[name], tag;

					for (var j = 0; j < res.length; j++) {
						if (res[j].type === "script" && alreadyAddedResources.indexOf(res[j].path) === -1) {
							alreadyAddedResources.push(res[j].path);
							tags += `<script src='${res[j].path}'${(res[j].avoid_merge || res[j].module ? ' avoid-merge' : '')}${(res[j].crossorigin ? ' crossorigin' : '')}${(res[j].module ? " type='module'" : '')}${(res[j].babel ? " type='text/babel'" : '')} ></script>`;
						} else if (res[j].type === "link" && alreadyAddedResources.indexOf(res[j].path) === -1) {
							alreadyAddedResources.push(res[j].path);
							tags += `<link rel='stylesheet' href='${res[j].path}'${(res[j].avoid_merge ? ' avoid-merge' : '')}${(res[j].crossorigin ? ' crossorigin' : '')} />`;
						}
					}

				} else {
					logger.warn(`Could not find resource: ${name}`);
				}

			}

			$this.replaceWith($(tags));

		});

		var localeFiles;

		if (this.resources.hasOwnProperty(lang)) {
			logger.debug(`Locale files found for locale (${lang.toUpperCase()})`);
			localeFiles = this.resources[lang.toUpperCase()];
		} else {
			logger.debug(`Could not find locale files for the current locale ${lang}. Loading defaults`);
			if (this.resources.hasOwnProperty(settings.template.defaultLanguage)) {
				localeFiles = this.resources[settings.template.defaultLanguage];
			} else {
				logger.warn(`Could not find default locale files. You must have a locale entry in the locales.json file for the default language`);
			}
		}

		// If the localeFiles array is there, then it's time to add the locale resource files to the template
		var tags2 = "";
		if (localeFiles) {
			// Loop through the files found for the current locale and insert them into the template the same way as resources.
			for (var i = 0; i < localeFiles.length; i++) {
				var localeFile = localeFiles[i];
				if (localeFile.type === "script") {
					tags2 += `<script src='${localeFile.path}' ${(localeFile.avoid_merge ? 'avoid-merge' : '')} ${(localeFile.crossorigin ? 'crossorigin' : '')} ></script>`;
				} else if (localeFile.type === "link") {
					tags2 += `<link rel='stylesheet' href='${localeFile.path}' ${(localeFile.avoid_merge ? 'avoid-merge' : '')} ${(localeFile.crossorigin ? 'crossorigin' : '')} />`;
				}
			}
			$("head").append($(tags2));
		}

	};

	private parseSessionTags(source:string, session:any):string {
		try {
			return source.replace(/\$sess{([a-z,_,\d]+)}/g, (match, key) => {
				try {
					return session.user[key] || "$sess%" + key + "%";
				} catch (ex) {
					return "";
				}
			}).replace(/\$session{([a-z,_,\d]+)}/g, (match, key) => {
				try {
					return session.user.session_data[key] || "$session%" + key + "%";
				} catch (ex) {
					return "";
				}
			});
		} catch (ex) {
			logger.error(ex);
			return source;
		}
	}

	private parseSecurityTags(source:any, session:any) {
		logger.info('Parsing security tags');
		try {
			return source.replace(/\$access{([a-z,_,\d,\/]+)}/g, function (match, function_name) {
				try {
					if (securityManager.hasAccess(session.user, function_name)) {
						return 'true';
					} else {
						return 'false';
					}
				}
				catch (ex) {
					return 'false';
				}
			});
		}
		catch (ex) {
			logger.error(ex);
			return source;
		}
	};

	private parseApplicationTags():void {
		//TODO: parse application based parameters into the template.
	}

	private parseCustomTags():void {
		//TODO: ability to extend the tag parsing functionality based on
	}

	private parseAccessControlTags = (source:string, session:any, level?:any):string => {

		let thisSource = source;

		try {
			// Check to see if the page source controls access tags set at a user
			// role level. If there are any included then we process them to determine
			// what will be included in the template.
			if (thisSource.search(/<exclude\s*.*>/) !== -1 || thisSource.search(/<include\s*.*>/) !== -1) {
				var $:CheerioStatic = cheerio.load(thisSource, {normalizeWhitespace: settings.cache.minifyHtml || true});
				var user_roles = JSON.parse(session.user.user_role_names);
				this.parseExcludeTags($, user_roles);
				this.parseIncludeTags($, user_roles);
				thisSource = $.html();
			}
			// Check to see if the page source contains access tags set on a functionality
			// level. If there are any included then we process them to determine
			// what will be included in the template.
			if (thisSource.search(/<access\s*.*>/) !== -1) {
				var $:CheerioStatic = cheerio.load(thisSource, {normalizeWhitespace: settings.cache.minifyHtml || true});
				this.parseAccessTags($, session.user);
				thisSource = $.html();
			}

			if (thisSource.search(/<noaccess\s*.*>/) !== -1) {
				var $:CheerioStatic = cheerio.load(thisSource, {normalizeWhitespace: settings.cache.minifyHtml || true});
				this.parseNoAccessTags($, session.user);
				thisSource = $.html();
			}

			if ((thisSource.search(/<exclude\s*.*>/) !== -1 || thisSource.search(/<include\s*.*>/) !== -1) ||
				(thisSource.search(/<access\s*.*>/) !== -1) ||
				(thisSource.search(/<noaccess\s*.*>/) !== -1)) {
					logger.warn("Processing embedded access control tags at level " + level);
					if (level < 3) {
						level++;
						return this.parseAccessControlTags(thisSource, session, level);
					} else {
						logger.warn("Document contains unreachable access control tags (commented out?).");
						return thisSource;
					}
			} else {
				return thisSource;
			}

		} catch (exception) {
			logger.error(`There was an error parsing access control tags.`, exception);
		}
	};
	private parseExcludeTags = ($:CheerioStatic, user_roles:string[]):void => {
		$("exclude").each(function (i, element) {
			var $this = $(this);
			var part_user_roles = $this.attr("user-roles").replace(/\s/g, '').split(",");
			for (var i = 0; i < part_user_roles.length; i++) {
				if (user_roles.indexOf(part_user_roles[i]) !== -1) { // If the user role in the exclude is found in the user' user_roles then it should be excluded
					$this.replaceWith('');
					return;
				}
			}
			$this.replaceWith($this.html()); // User has access
		});
	};
	private parseIncludeTags = ($:CheerioStatic, user_roles:string[]):void=> {
		$("include").each(function (i, element) {
			var $this = $(this);
			var part_user_roles = $this.attr("user-roles").replace(/\s/g, '').split(",");
			//console.log("<INCLUDE>", part_user_roles);
			for (var i = 0; i < part_user_roles.length; i++) {
				if (user_roles.indexOf(part_user_roles[i]) !== -1) { // If the user role in the include is found in the user' user_roles
					$this.replaceWith($this.html()); // User has access.
					return;
				}
			}
			$this.replaceWith('');
		});
	};
	private parseAccessTags = ($:CheerioStatic, user:any):void=> {
		$("access").each(function (i, element) {
			var $this = $(this);
			var part_functions = $this.attr("functions").replace(/\s/g, '').split(",");
			part_functions.forEach(function (function_name) {
				// If the user has access to this function then we will insert the
				// elements between the access tags.
				if (securityManager.hasAccess(user, function_name)) {
					$this.replaceWith($this.html());
					return;
				}

			});
			// User has no access so the content is removed
			$this.replaceWith('');
		});
	};

	private parseNoAccessTags = ($:CheerioStatic, user:any):void=> {
		$("noaccess").each(function (i, element) {
			var $this = $(this);
			var part_functions = $this.attr("functions").replace(/\s/g, '').split(",");
			part_functions.forEach(function (function_name) {
				// If the user has access to this function then we will insert the
				// elements between the access tags.
				if (!securityManager.hasAccess(user, function_name)) {
					$this.replaceWith($this.html());
					return;
				}

			});
			// User has no access so the content is removed
			$this.replaceWith('');
		});
	};

	private parseResources($:CheerioStatic, template_name:string, language:string, session:any):void {

		logger.debug(`Parsing resources for ${template_name}`);

		// Parse Script resources
		var $head = $("head");
		var scripts:string = "";
		var stylesheets:string = "";
		var name = language.toLowerCase() + "_" + template_name.replace(/\//g, '');

		// logger.trace(`Parsing script tags`);
		$("script").each(function (i, element) {
			var $this = $(this);
			logger.trace(`Processing resource ${$this}`);
			if ($this.attr("src").startsWith('http')) {
				logger.debug(`External resource ${$this.attr("src")} is included but not injected in the compressed JavaScript`);
			} else if ($this.attr("type") === "module") {
				logger.debug(`Injecting ${$this.attr("src")} as a module`);
			} else if ($this.attr("type") === "text/babel") {
				logger.debug(`Injecting ${$this.attr("src")} as babel`);
			} else if (typeof $this.attr("avoid-merge") === "undefined" && typeof $this.attr("src") !== "undefined") {
				try {
					scripts += fs.readFileSync(processLocation + "/../" + $this.attr("src").replace("/", ""), "UTF-8") + "\n";
					$this.remove();
				} catch (exception) {
					logger.error(`Could not read resource file: ${$this.attr("src").replace("/", "")}.`, exception);
				}
			}
		});
		// logger.trace(`Parsed script tags`);

		// Insert script template
		var scriptPath:string = processLocation + "/" + settings.template.assetPath + "/scripttemplates" + template_name + ".js";

		try {

			var mainScriptTemplate = fs.readFileSync(scriptPath, "UTF-8") + "\n";

			scripts += mainScriptTemplate;

			// logger.debug(`Processing script tag ${input}`);
			scripts = (input => {
				return this.parseScriptContent(input, template_name, language, session);
			})(scripts);

		} catch (error) {
			logger.error(`Could not locate script file for template ${template_name}. Check the scripttemplates ${template_name}.js file.`, error);
		}

		$("link").each(function (i, element) {
			var $this = $(this);
			if (typeof $this.attr("avoid-merge") === "undefined" && typeof $this.attr("href") !== "undefined") {
				try {
					stylesheets += fs.readFileSync(processLocation + "/../" + $(this).attr("href"), "UTF-8");
				} catch (exception) {
					logger.error(`Could not read resource file: ${$this.attr("href")}.`, exception);
				}
				$this.remove();
			}
		});

		if (fs.existsSync(processLocation + "/../public/generated/js" + name + ".js")) {
			fs.removeSync(processLocation + "/../public/generated/js" + name + ".js");
			fs.writeFileSync(processLocation + "/../public/generated/js" + name + ".js", scripts);
		} else {
			fs.writeFileSync(processLocation + "/../public/generated/js" + name + ".js", scripts);
		}
		if (fs.existsSync(processLocation + "/../public/generated/css" + name + ".css")) {
			fs.removeSync(processLocation + "/../public/generated/css" + name + ".css");
			fs.writeFileSync(processLocation + "/../public/generated/css" + name + ".css", stylesheets);
		} else {
			fs.writeFileSync(processLocation + "/../public/generated/css" + name + ".css", stylesheets);
		}

		$head
			.append("<link rel='stylesheet' href='/public/generated/css" + name + ".css'>")
			.append("<script src='/public/generated/js" + name + ".js'></script>");
	}

	private parseScriptContent = (input:string, template_name:string, language:string, session:any):string => {

		// Inject script parts
		try {

			logger.debug(`Processing script part tags`);
			input = input.replace(/\$part{\'([a-z,@,\-,.,_,\/,\d]+)\'}/g, (match, key) => {

				let keyParts = key.split(",");

				// Check to see if the tag has access tokens and if it does then check
				// that the current user has access to that function.
				if (keyParts.length > 1) {
					logger.debug(`Processing script part tag ${keyParts[0]} with access permissions ${keyParts[1]}`);
					if (!securityManager.hasAccess(session.user, keyParts[1])) {
						logger.error(`Script Part ${keyParts[0]} was not injected as the user has no access to ${keyParts[1]}`);
						return '';
					}
				} else {
					logger.debug(`Processing script part tag ${keyParts[0]}`);
				}

				// Remove any loading / from the injected part
				keyParts[0].replace(/^\/+/g, '');

				try {
					return this.parseScriptContent(fs.readFileSync(processLocation + "/" + keyParts[0] + ".js", "UTF-8"), template_name, language, session) + "\n";
				} catch (ex) {
					logger.error(`Script Part ${keyParts[0]} was not injected as the file was not found`);
					logger.error(ex);
					return `Script Part ${keyParts[0]} was not injected as the file was not found`;
				}

			});

			// Replace information from the user's record
			input = input.replace(/\$user{([a-z,_,\d]+)}/g, (match, key) => {
				try {
					var item = session.user[key];
					if (item) {
						return item;
					} else {
						return "";
					}
				} catch (ex) {
					return "%user: " + key + "%";
				}
			});

			// Replace information from the user's record
			input = input.replace(/\$user{([a-z,_,\d]+)}/g, function (match, key) {
				try {
					var item = session.user[key];
					if (item) {
						return item;
					}
					else {
						return "";
					}
				}
				catch (ex) {
					return "%user: " + key + "%";
				}
			});

			// logger.debug(`Processing session tags ${input}`);
			input = this.parseSessionTags(input, session);

			// logger.debug(`Processing security tags ${input}`);
			input = this.parseSecurityTags(input, session);

			// logger.debug(`Processing language tags ${input}`);
			input = input.replace(/\$language{}/g, function (match, key) {
				return session.language.toLowerCase() || settings.template.defaultLanguage;
			});

			// Replace information from the settings file
			// logger.debug(`Processing setting tags ${input}`);
			input = input.replace(/\$setting{([a-z,_,\d]+)}/g, (match, key) => {
				switch (key) {
					case 'faye_server':
						return settings.faye.server;
					case 'linked_sites':
						return settings.linked_sites;
					default:
						if (settings.user.hasOwnProperty(key)) {
							return settings.user[key];
						} else {
							return "%setting: " + key + "%";
						}
				}
			});

			// Replace dictionary items
			// logger.debug(`Processing dictionary tags ${input}`);
			input = input.replace(/\$dict{([a-z,_,\d]+)}/g, (match, key) => {
				var dictItem = dictionaryManager.getDictionaryItem(language, key);
				if (dictItem !== null)
					return dictItem;
				return "%" + key + "%";
			});

			// logger.debug(`Processing resource tags ${input}`);
			input = input.replace(/\$resource\{([a-z,_,\.,\d]+)\}/g, (match, key) => {
				let filePath = Resources.getResourceFilePath(key);
				if (filePath) {
					return filePath;
				}
				return `%${key}%`;
			});

			// logger.debug(`Processing templatename tags ${input}`);
			input = input.replace(/\$templatename\{\}/g, match => template_name);

		} catch (ex) {
			logger.error(ex);
		}
		return input;
	};

	private getCacheInterval = function ($:CheerioStatic):number {

		var timeout:number = (settings.cache.defaultCacheInterval.seconds * 1000) +
			(settings.cache.defaultCacheInterval.minutes * 60000) +
			(settings.cache.defaultCacheInterval.hours * 3600000) +
			(settings.cache.defaultCacheInterval.days * 86400000) +
			(settings.cache.defaultCacheInterval.weeks * 604800000);
		var templateLevelCache = $("cache").attr("timeout");

		$("cache").each(function (i, element) {
			$(element).replaceWith("");
		});

		// If the user defined a <cache timeout></cache> element somewhere in the template, then parse the string from the timeout property and convert it to milliseconds.
		if (typeof templateLevelCache !== "undefined") {
			var weeks = templateLevelCache.match(/(\d+)w/) ? parseInt(templateLevelCache.match(/(\d+)w/)[1]) : 0;
			var days = templateLevelCache.match(/(\d+)d/) ? parseInt(templateLevelCache.match(/(\d+)d/)[1]) : 0;
			var hours = templateLevelCache.match(/(\d+)h/) ? parseInt(templateLevelCache.match(/(\d+)h/)[1]) : 0;
			var minutes = templateLevelCache.match(/(\d+)m/) ? parseInt(templateLevelCache.match(/(\d+)m/)[1]) : 0;
			var seconds = templateLevelCache.match(/(\d+)s/) ? parseInt(templateLevelCache.match(/(\d+)s/)[1]) : 0;
			timeout = (seconds * 1000) +
				(minutes * 60000) +
				(hours * 3600000) +
				(days * 86400000) +
				(weeks * 604800000);
			logger.info(`Caching template for ${weeks} weeks ${days} days ${hours} hours ${minutes} minutes ${seconds} seconds`);
		}

		return timeout;

	};

	public getTemplate = (template_name:string, lang:string, session:any):ITemplate => {

		if (this.templateCache.isCached(template_name, lang)) {

			logger.debug(`Loading template ${template_name} from cache`);
			var template = this.templateCache.get(template_name, lang);
			template.source = this.parseSessionTags(template.source, session);
			template.source = this.parseAccessControlTags(template.source, session);
			return template;

		} else {

			var $ = this.$;

			// Load the template file to check the <base-template> element
			logger.debug(`Loading template ${template_name} from disk`);

			if (fs.existsSync(this.asset_path + template_name + ".html")) {
				let mainPart:string = fs.readFileSync(this.asset_path + template_name + ".html", "UTF-8");

				let matches = mainPart.match(/<base-template[\s*]name="(.+)">/);

				// Remove the unnecessary resource tag from the template source.
				mainPart = mainPart.replace(/<base-template[\s*]name="(.*)"><\/base-template>/, '');

				let baseTemplate:string;
				if (matches && matches.length > 1 && fs.existsSync(this.asset_path + matches[1] + ".html")) {
					baseTemplate = fs.readFileSync(this.asset_path + matches[1] + ".html", "UTF-8");
				} else {
					baseTemplate = this.base_template;
					logger.info(`Base template not defined or missing for (${template_name}). The default base template (${settings.template.defaultBaseTemplateName}) will be used.`);
				}

				$ = cheerio.load(baseTemplate, {normalizeWhitespace: settings.cache.minifyHtml || true});

				//$("main-part").replaceWith(fs.readFileSync(this.asset_path + template_name + ".html", "UTF-8"));
				$("main-part").replaceWith(mainPart);

				//<editor-fold desc="Parse cacheable parts">
				this.parsePartTags($);

				this.parseDictionaryTags($, lang); // <dict key=""/>

				// logger.debug(`parseResourceTags`);
				this.parseResourceTags($, lang); // <resource name="..."></resource>

				// logger.debug(`parseResources`);
				this.parseResources($, template_name, lang, session); // Script, CSS

				// logger.debug(`Retrieve the template source`);
				var templateSource = $.html(); // Get the html as a string from cheerio
				// logger.debug(`Retrieved the template source: ${templateSource}`);

				// Replace $dict{key} values from the dictionary
				// logger.debug(`Dictionary Replacement`);
				templateSource = templateSource.replace(/\$dict{([a-z,_,\d]+)}/g, (match, key) => {
					var dictItem = dictionaryManager.getDictionaryItem(lang, key);
					if (dictItem !== null)
						return dictItem;
					return "%" + key + "%";
				});

				// logger.debug(`Language Replacement`);
				templateSource = templateSource.replace(/\$language{}/g, (match, key) => {
					return session.language.toLowerCase() || settings.template.defaultLanguage;
				});
				//</editor-fold>

				templateSource = templateSource.replace(/\$resource\{([a-z,_,\.,\d]+)\}/g, (match, key) => {
					let filePath = Resources.getResourceFilePath(key);
					if (filePath) {
						return filePath;
					}
					return `%${key}%`;
				});

				templateSource = templateSource.replace(/\$templatename\{\}/g, match => template_name);

				templateSource = this.parseAccessControlTags(templateSource, session);

				// Cache the current state of the template
				var tpl:ITemplate = {
					lang:           lang,
					source:         templateSource,
					template_name:  template_name,
					cache_interval: this.getCacheInterval($)
				};
				this.templateCache.put(Utils.copy(tpl));


				// Parse session variables using: $sess{key}
				tpl.source = this.parseSessionTags(tpl.source, session);

				tpl.source = tpl.source.replace(/<cache[\s*]timeout="(.*)"><\/cache>/, '');

				return tpl;

			} else return null;
		}
	};

	private loadResources() {
		logger.debug("Loading resources for generating resource tags");
		let resources = {};
		let locales = {};
		try {
			resources = JSON.parse(fs.readFileSync(processLocation + "/../data/resources.json", "UTF-8"));
		} catch (ex) {
			logger.error("Failed to load resources.json. Please make sure that the file exist and its valid JSON", ex);
		}
		try {
			var defaultFile = parseResourceFile('../locales/default.json', null, true);
			if (defaultFile == null) {
				throw new Error('There was an error loading the default resource file');
			}

			let systemSpecificFile = fs.existsSync(process.argv[3]) ? parseResourceFile(process.argv[3], {}, false) : parseResourceFile("../locales/" + HOSTNAME + ".json", {}, false);

			logger.info('Deep extended default locales with system specific locales');
			let combinedFile = deepExtend(defaultFile, systemSpecificFile);
			for (var key in combinedFile) {
				logger.info("Resources loaded for locale (" + key.toUpperCase() + ")");
				resources[key.toUpperCase()] = combinedFile[key.toUpperCase()];
			}
		} catch (ex) {
			logger.error("Failed to load locales.json", ex);
		}
		return resources;
	}

	private compressResources(resources):any {
		try {
			logger.info("Compressing resource files...");
			var outputFolder = processLocation + "/../compressed/";
			fs.emptyDirSync(outputFolder);
			logger.debug("Output folder for compressed resources cleared");

			var totalCompressed = 0;
			for (var key in resources) {
				var resource = resources[key];
				for (var i = 0; i < resource.length; i++) {
					if (resource[i].type === "script" && (!resource[i].hasOwnProperty("uglify") || resource[i].uglify === true)) {
						var filename = uuid.v4();
						var relativePath = "/compressed/" + filename;
						var minifiedFilePath = outputFolder + filename;
						logger.debug("Compressing: " + resource[i].path);
						fs.writeFileSync(minifiedFilePath, UglifyJS.minify(processLocation + "/.." + resource[i].path, {
							// TODO: Link compression settings to the settings factory.
							compress: {
								sequences:     true,  // join consecutive statemets with the “comma operator”
								properties:    false,  // optimize property access: a["foo"] → a.foo
								dead_code:     true,  // discard unreachable code
								drop_debugger: true,  // discard “debugger” statements
								unsafe:        false, // some unsafe optimizations (see below)
								conditionals:  true,  // optimize if-s and conditional expressions
								comparisons:   true,  // optimize comparisons
								evaluate:      true,  // evaluate constant expressions
								booleans:      true,  // optimize boolean expressions
								loops:         true,  // optimize loops
								unused:        true,  // drop unused variables/functions
								hoist_funs:    true,  // hoist function declarations
								hoist_vars:    false, // hoist variable declarations
								if_return:     true,  // optimize if-s followed by return/continue
								join_vars:     true,  // join var declarations
								cascade:       true,  // try to cascade `right` into `left` in sequences
								side_effects:  true,  // drop side-effect-free statements
								warnings:      false  // warn about potentially dangerous optimizations/code
							}
						}, true).code);
						totalCompressed++;
						resource[i].path = relativePath;
					} else if (resource[i].type === "link" && (!resource[i].hasOwnProperty("uglify") || resource[i].uglify === true)) {
						var filename = uuid.v4();
						var relativePath = "/compressed/" + filename;
						var minifiedFilePath = outputFolder + filename;
						logger.debug("Compressing: " + resource[i].path);
						fs.writeFileSync(minifiedFilePath, UglifyCSS.processFiles([processLocation + "/.." + resource[i].path], {
							//   compress: settings.cache.javascript
						}));
						totalCompressed++;
						resource[i].path = relativePath;
					}
				}
			}
			logger.info(totalCompressed + " files were succesfully compressed");
			return resources;
		} catch (e) {
			logger.error("Failed to compress resources.", e);
		}
	}

	constructor(asset_path:string) {

		this.asset_path = processLocation + "/" + settings.template.assetPath + "/pagetemplates/";
		test = asset_path;
		this.base_template = fs.readFileSync(this.asset_path + settings.template.defaultBaseTemplateName + ".html", "UTF-8");
		this.templateCache = new TemplateCache(60);

		var res = this.loadResources();
		if (settings.template.compressResources) {
			this.resources = this.compressResources(res);
		} else {
			this.resources = res;
		}
		dictionaryManager.loadDictionary();
	}

}

/*
H E L P E R   F U N C T I O N S
*/
function deepExtend() {
	var args = [];
	for (var _i = 0; _i < arguments.length; _i++) {
		args[_i - 0] = arguments[_i];
	}
	var target = args[0];
	var val, src, clone;
	args.forEach(function (obj) {
		if (typeof obj !== 'object' || Array.isArray(obj)) {
			return;
		}
		Object.keys(obj).forEach(function (key) {
			src = target[key];
			val = obj[key];
			if (val === target) {
				return;
			}
			else if (typeof val !== 'object' || val === null) {
				target[key] = val;
				return;
			}
			else if (Array.isArray(val)) {
				target[key] = deepCloneArray(val);
				return;
			}
			else if (isSpecificValue(val)) {
				target[key] = cloneSpecificValue(val);
				return;
			}
			else if (typeof src !== 'object' || src === null || Array.isArray(src)) {
				target[key] = deepExtend({}, val);
				return;
			}
			else {
				target[key] = deepExtend(src, val);
				return;
			}
		});
	});
	return target;
}

function deepCloneArray(arr) {
	var clone = [];
	arr.forEach(function (item, index) {
		if (typeof item === 'object' && item !== null) {
			if (Array.isArray(item)) {
				clone[index] = deepCloneArray(item);
			}
			else if (isSpecificValue(item)) {
				clone[index] = cloneSpecificValue(item);
			}
			else {
				clone[index] = deepExtend({}, item);
			}
		}
		else {
			clone[index] = item;
		}
	});
	return clone;
}

function isSpecificValue(val) {
	return !!(val instanceof Buffer
		|| val instanceof Date
		|| val instanceof RegExp);
}

function cloneSpecificValue(val) {
	if (val instanceof Buffer) {
		var x = new Buffer(val.length);
		val.copy(x);
		return x;
	}
	else if (val instanceof Date) {
		return new Date(val.getTime());
	}
	else if (val instanceof RegExp) {
		return new RegExp(val);
	}
	else {
		throw new Error('Unexpected situation');
	}
}

function parseResourceFile(filePath, defaultValue, logError) {
	var tmp;
	try {
		logger.info('Loading resource file: ' + filePath);
		tmp = JSON.parse(fs.readFileSync(filePath, 'utf8'));
	}
	catch (error) {
		if (logError) {
			logger.error('Error while loading locale files from file system: ' + filePath);
			logger.debug(error);
		}
		return defaultValue;
	}
	return tmp;
}
