/*
 ____________                                           _
 |___  /  ____|                                        | |
   / /| |__ _ __ __ _ _ __ ___   _____      _____  _ __| | __
  / / |  __| '__/ _` | '_ ` _ \ / _ \ \ /\ / / _ \| '__| |/ /
 / /__| |  | | | (_| | | | | | |  __/\ V  V / (_) | |  |   <
/_____|_|  |_|  \__,_|_| |_| |_|\___| \_/\_/ \___/|_|  |_|\_\

Author:		Forisz
Date:		06/04/15.

Utility functions for the Framework application
*/

///<reference path='../tsc-descriptors/colors.d.ts'/>
///<reference path='../tsc-descriptors/moment.d.ts'/>
///<reference path='../tsc-descriptors/sjcl.d.ts'/>
///<reference path='../tsc-descriptors/faye.d.ts'/>

import colors = require("colors");
import moment = require("moment");
import sjcl = require("sjcl-full");
import faye = require("faye");
var hostname = require("os").hostname();

export enum Severity {
	log, low, medium, high
}

export class Logger {

	private environment:string;

	private levels:any;

	log = (message, severity?) => {

		var logDate = moment();
		var messageData = "";

		if (Object.prototype.toString.call(message) == '[object Array]') {
			for (var i = 0; i < message.length; i++) {
				if (Object.prototype.toString.call(message[i]) == '[object Object]') {
					messageData += JSON.stringify(message[i]);
				} else {
					messageData += message[i];
				}
			}
		} else {
			if (Object.prototype.toString.call(message) == '[object Object]') {
				messageData = JSON.stringify(message);
			} else {
				messageData = message;
			}
		}

		console.log(
			logDate.format("YYYY-MM-DD HH:mm:ss.SSS") +
			" [" + this.environment + "] " +
			this.levels[severity ? severity : Severity.log](messageData)
		)
	};

	constructor(environment:string, level?:number) {
		this.environment = environment;
		this.levels = {};
		this.levels[Severity.log] = function (message) {
			return message
		};
		this.levels[Severity.low] = function (message) {
			return colors.gray(message)
		};
		this.levels[Severity.medium] = function (message) {
			return colors.yellow(message)
		};
		this.levels[Severity.high] = function (message) {
			return colors.red(message)
		};

	}
}

export function generateSalt(bytes) {
	var buffer = crypto.randomBytes(bytes), hexa = "";
	for (var i = 0; i < buffer.length; i++) {
		var byte = buffer[i].toString(16);
		if (byte.length === 1) {
			byte = "0" + byte;
		}
		hexa += byte;
	}
	return hexa;
}

export function checkPasswordMatch(password, hash) {
	var salt = hash.substr(hash.length - 126),
		hashed_password = sjcl.codec.hex.fromBits(sjcl.hash.sha512.hash(password + salt));
		//console.log((hashed_password + salt) === hash, salt);
	return ((hashed_password + salt) === hash);
};

export function copy(obj) {
	return JSON.parse(JSON.stringify(obj));
}
